'use strict';

const express = require('express');
const config = require('./config/environment');
const logger = require('./helpers/logger');
const seed = require('./config/seed');

const app = express();

module.exports = app;

require('./config/uncaught-error-handler');
require('./config/mongoose');
require('./config/express')(app);
require('./routes')(app);

// if (config.seedDB) {
//   switch (config.env) {
//     case 'development':
//       seed.seedDB();
//       break;
//     case 'production':
//       seed.seedDBProduction();
//       break;
//   }
// }

require('http')
  .createServer(app)
  .listen(config.port, () => {
    logger.info('Express server listening on %d, in %s mode', config.port, config.env);

    if (config.env === 'development') {
      require('ripe').ready();
    }
  })
  .on('error', e => {
    logger.error(e);
    process.exit(1);
  });
