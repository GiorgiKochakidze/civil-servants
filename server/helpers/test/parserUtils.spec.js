'use strict';

const chai = require('chai');
const {expect} = chai;
const parserUtils = require('../parserUtils');


describe('parserUtils', () => {
  describe('#parseOffsetAndLimit()', () => {
    describe('limit', () => {
      it('should return 5 when limit is 5', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: 5});
        expect(parsed).to.have.property('limit', 5);
      });

      it('should return 1000 when all is true', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: 5, all: 'true'});
        expect(parsed).to.have.property('limit', 1e3);
      });

      it('should return 0 when limit is larger than max value', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: 1005});
        expect(parsed).to.have.property('limit', 0);
      });

      it('should return 0 when limit is a negative number', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: -1});
        expect(parsed).to.have.property('limit', 0);
      });

      it('should return 0 when limit is not passed', () => {
        const parsed = parserUtils.parseOffsetAndLimit({});
        expect(parsed).to.have.property('limit', 0);
      });

      it('should return 0 when limit is null', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: null});
        expect(parsed).to.have.property('limit', 0);
      });

      it('should return 0 when limit is NaN', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: NaN});
        expect(parsed).to.have.property('limit', 0);
      });

      it('should return 0 when limit is Infinity', () => {
        const parsed = parserUtils.parseOffsetAndLimit({limit: Infinity});
        expect(parsed).to.have.property('limit', 0);
      });
    });

    describe('offset', () => {
      it('should return according to formula (page - 1) * limit', () => {
        const parsed = parserUtils.parseOffsetAndLimit({page: 153, limit: 97});
        expect(parsed).to.have.property('offset', 14744);
      });

      it('should return 0 when all is true', () => {
        const query = {page: 153, limit: 97, all: 'true'};
        const parsed = parserUtils.parseOffsetAndLimit(query);
        expect(parsed).to.have.property('offset', 0);
      });

      it('should return 0 if malformed page/limit are passed', () => {
        const parsed = parserUtils.parseOffsetAndLimit({page: NaN});
        expect(parsed).to.have.property('offset', 0);
      });
    });
  });

  describe('#parseSortBy()', () => {
    it('should have value 1 if sign is +', () => {
      const sortBy = 'createdAt+';
      const parsed = parserUtils.parseSortBy(sortBy);
      expect(parsed).to.have.property('value', 1);
    });

    it('should have value -1 if sign is -', () => {
      const sortBy = 'createdAt-';
      const parsed = parserUtils.parseSortBy(sortBy);
      expect(parsed).to.have.property('value', -1);
    });

    it('should return valid key', () => {
      const sortBy = 'createdAt-';
      const parsed = parserUtils.parseSortBy(sortBy);
      expect(parsed).to.have.property('key', 'createdAt');
    });

    it('should return null if passed param is not a string', () => {
      const sortBy = null;
      const parsed = parserUtils.parseSortBy(sortBy);
      expect(parsed).to.equal(null);
    });

    it('should return null if passed param does not end with a sign', () => {
      const sortBy = 'createdAt';
      const parsed = parserUtils.parseSortBy(sortBy);
      expect(parsed).to.equal(null);
    });
  });

  describe('#parseArray()', () => {
    it('should parse an array', () => {
      const array = ['a', 'b', 'c'];
      expect(parserUtils.parseArray(array)).deep.equal(array);
    });

    it('should wrap string into an array', () => {
      expect(parserUtils.parseArray('xyz')).deep.equal(['xyz']);
    });

    it('should return an empty array if data is falsy', () => {
      expect(parserUtils.parseArray(false)).deep.equal([]);
      expect(parserUtils.parseArray(null)).to.deep.equal([]);
      expect(parserUtils.parseArray(undefined)).to.deep.equal([]);
      expect(parserUtils.parseArray(0)).to.deep.equal([]);
    });

    it('should return an empty array if data is truthy but not array', () => {
      expect(parserUtils.parseArray(true)).deep.equal([]);
      expect(parserUtils.parseArray({})).to.deep.equal([]);
      expect(parserUtils.parseArray(8)).to.deep.equal([]);
    });
  });
});
