'use strict';

const _ = require('lodash');
const uuid = require('node-uuid');


module.exports = {
  cloneStub
};


function cloneStub(stub, uniqueFields) {
  uniqueFields = uniqueFields || [];
  stub = _.cloneDeep(stub);

  for (const field of uniqueFields) {
    const dot = field.indexOf('.');

    if (dot !== -1) {
      const field1 = field.substring(0, dot);
      const field2 = field.substring(dot + 1);
      stub[field1][field2] = `test-${uuid.v4()}`;
    } else {
      stub[field] = `test-${uuid.v4()}`;
    }
  }
  return stub;
}
