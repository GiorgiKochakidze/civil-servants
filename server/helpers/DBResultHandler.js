'use strict';

const {ResourceNotFoundError} = require('../errors');


module.exports = {
  assertFound: message => assertFound.bind(null, message)
};


function assertFound(message, result) {
  if (result === null) {
    throw new ResourceNotFoundError(message);
  }
  return result;
}
