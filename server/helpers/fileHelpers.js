'use strict';

var path = require('path');
var config = require('../config/environment');

module.exports = {
  getFilePath,
  getFileUrl,
};


function getFilePath(filename) {
  return path.join(config.paths.uploads, filename);
}

function getFileUrl(filename) {
  return `${config.url.scheme}://${config.url.host}/${filename}`;
}
