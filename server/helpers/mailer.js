'use strict';

const nodemailer = require('nodemailer');
const config = require('../config/environment');
const logger = require('../helpers/logger');

const transporter = nodemailer.createTransport({
  service: config.company.email.service,
  auth: {
    user: config.company.email.user,
    pass: config.company.email.password
  }
});


module.exports = {
  send,
  sendHtml
};


transporter.verify()
  .then(() => {
    logger.info(`Mailer ${config.company.email.user} verified`);
  })
  .catch((error) => {
    logger.error(error);
  });

function send(to, subject, text) {
  const mailOptions = {
    from: config.company.email.user,
    to,
    text,
    subject
  };
  return transporter.sendMail(mailOptions)
    .catch(handleError);
}

function sendHtml(to, subject, html) {
  const mailOptions = {
    from: config.company.email.user,
    to,
    html,
    subject
  };
  return transporter.sendMail(mailOptions)
    .catch(handleError);
}

function handleError(error) {
  logger.error('Failed to send email', error);
}
