'use strict';

const _ = require('lodash');

const MAX_LIMIT = 1e3;


module.exports = {
  parseOffsetAndLimit,
  parseSortBy,
  parseArray
};


function parseOffsetAndLimit({page, limit, all}) {
  all = all === 'true';
  return {
    offset: all ? 0 : computeOffset(parsePage(page), parseLimit(limit)),
    limit: all ? MAX_LIMIT : parseLimit(limit)
  };
}

function parsePage(page) {
  page = Number(page);
  return (isFinite(page) && page > 0) ? page : 1;
}

function parseLimit(limit) {
  limit = Number(limit);
  return (isFinite(limit) && limit > 0 && limit <= MAX_LIMIT) ? limit : 0;
}

function computeOffset(page, limit) {
  return (page - 1) * limit;
}

function parseSortBy(sortBy) {
  sortBy = String(sortBy);
  const key = sortBy.substr(0, sortBy.length - 1);
  const sign = sortBy[sortBy.length - 1];
  let value;

  switch (sign) {
    case '+':
      value = 1;
      break;
    case '-':
      value = -1;
      break;
    default:
      return null;
  }

  return {
    key,
    value
  };
}

function parseArray(data) {
  if (data instanceof Array) {
    return data;
  } else if (typeof data === 'string') {
    return [data];
  } else {
    return [];
  }
}
