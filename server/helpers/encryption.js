'use strict';

const Promise = require('bluebird');
const encrypt = require('bcrypt-nodejs');

Promise.promisifyAll(encrypt);


module.exports = {
  generateHash,
  compareHash
};


function generateHash(data) {
  return encrypt.hashAsync(data, null, null);
}

function compareHash(data, hash) {
  return encrypt.compareAsync(data, hash);
}
