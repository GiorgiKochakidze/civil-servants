'use strict';

module.exports = {
  roles: {
    GUEST: 'guest',
    USER: 'user',
    ADMIN: 'admin'
  }
};
