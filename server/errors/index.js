'use strict';

class AppError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
  }
}

class ResourceNotFoundError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 404;
  }
}

class UnauthorizedError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 401;
  }
}

class ValidationError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 400;
  }
}

module.exports = {
  AppError,
  ResourceNotFoundError,
  UnauthorizedError,
  ValidationError
};
