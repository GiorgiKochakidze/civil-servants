'use strict';

const _ = require('lodash');
const router = require('express').Router();
const co = require('co');
const fs = require('fs');
const fbGraph = require('fbgraph');
const FileHelpers = require('../../helpers/fileHelpers');
const Question = require('./question.dao');
const QuestionExcel = require('./question.excel');
const parser = require('./question.parser');
const auth = require('../../auth');

module.exports = router;


router.get('/', auth.isAdmin, parser.parseGetByQuery, getByQuery);
router.get('/active', getActiveQuestion);
router.get('/:questionId/export/excel', auth.isAdmin, exportToExcel);

router.post('/', auth.isAdmin, parser.parseCreate, create);
router.post('/update', auth.isAdmin, parser.parseUpdate, update);
router.post('/:questionId/toggleStatus', auth.isAdmin, toggleStatus);
router.post('/:questionId/answer', answer);

router.delete('/:questionId', auth.isAdmin, destroy);


// =============== GET ===============

function getByQuery(req, res, next) {
  co(function* () {
    const query = req.parsed;
    const questionsData = yield Question.getByQuery(query);
    res.json(questionsData);
  })
  .catch(next);
}

function getActiveQuestion(req, res, next) {
  co(function* () {
    const question = yield Question.getActiveQuestion();
    res.json(question);
  }).catch(next);
}

function exportToExcel(req, res, next) {
  co(function* () {
    const questionId = req.params.questionId;
    const fileName = getFileRandomName() + '.xlsx';
    const filePath = FileHelpers.getFilePath(fileName);
    const fileUrl = FileHelpers.getFileUrl(fileName);
    const sheetName = 'survey';

    const question = yield Question.getById(questionId);
    yield QuestionExcel.write(question, sheetName, filePath);

    setTimeout(() => {
      fs.unlink(filePath, () => {});
    }, 60 * 1000);

    res.json({url: fileUrl});
  }).catch(next);
}

function getFileRandomName() {
  return Math.random().toString(36).substring(7) + new Date().getTime();
}

// =============== POST ===============

function create(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield Question.create(payload);
    res.sendStatus(201);
  })
  .catch(next);
}

function update(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield Question.update(payload._id, payload);
    res.sendStatus(200);
  })
  .catch(next);
}

function toggleStatus(req, res, next) {
  co(function* () {
    const questionId = req.params.questionId;
    const questionToToggle = (yield Question.getById(questionId));
    const questions = yield Question.getAll();
    for (const question of questions) {
      yield Question.update(question._id, {active: false});
    }
    yield Question.update(questionToToggle._id, {active: !questionToToggle.active});
    res.sendStatus(200);
  })
  .catch(next);
}

function parseFacebookUser(req, res, next) {
  const input_token='1736511370007267|85bf7aaf541ac9a64a00dae0c30e0009';
  const access_token= req.body.accessToken;
  const userId = req.body.userID;
  const fields = 'id,first_name,last_name,gender,birthday,age_range';
  const queryUrl = `${userId}?fields=${fields}&input_token=${input_token}&access_token=${access_token}`;

  fbGraph.get(queryUrl, (err, data) => {
    if (err) {
      return res.sendStatus(200);
    } else {
      req.body.respondent = {
        facebookId: data.id,
        firstName: data.first_name,
        lastName: data.last_name,
        gender: data.gender,
        birthday: data.birthday,
        age: data.age_range.min || data.age_range.max
      };
      next();
    }
  });
}

function answer(req, res, next) {
  co(function* () {
    const respondent = _.pick(req.body, ['firstName', 'lastName', 'gender', 'ageRange', 'position']);
    const questionId = req.params.questionId;
    const question = yield Question.getById(questionId);
    const answers = req.body.answers.slice(0, question.answers.length);
    for (let i = 0; i < answers.length; i++) {
      if (answers[i] === true) {
        const answerId = question.answers[i]._id;
        yield Question.addAnswer(questionId, answerId, respondent);
      }
    }
    res.sendStatus(200);
  }).catch(next);
}

function userAnsweredQuestion(question, currentRespondent) {
  for (const answer of question.answers) {
    for (const respondent of answer.respondents) {
      if (respondent.facebookId === currentRespondent.facebookId) {
        return true;
      }
    }
  }
  return false;
}

function destroy(req, res, next) {
  co(function* () {
    const {questionId} = req.params;
    yield Question.destroy(questionId);
    res.sendStatus(200);
  })
  .catch(next);
}
