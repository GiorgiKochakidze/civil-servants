'use strict';

let Excel = require('exceljs');

module.exports = {
  write
};


function write(question, sheetname, filename) {
  const workbook = new Excel.Workbook();
  workbook.addWorksheet(sheetname);
  const worksheet = workbook.getWorksheet(1);
  worksheet.columns = [
    { header: 'First Name', key: 'firstName', width: 15 },
    { header: 'Last Name', key: 'lastName', width: 15 },
    { header: 'Gender Geo', key: 'genderGeo', width: 10 },
    { header: 'Gender Eng', key: 'genderEng', width: 10 },
    { header: 'Age Geo', key: 'ageGeo', width: 10 },
    { header: 'Age Eng', key: 'ageEng', width: 10 },
    { header: 'Position Geo', key: 'positionGeo', width: 15 },
    { header: 'Position Eng', key: 'positionEng', width: 15 },
    { header: 'Date', key: 'createdAt', width: 10 },
    { header: 'Admin Votes', key: 'votes', width: 10 },
    { header: 'Answer Geo', key: 'answerGeo', width: 30 },
    { header: 'Answer Eng', key: 'answerEng', width: 30 }
  ];

  question.answers.forEach((answer) => {
    answer.respondents.forEach((respondent) => {
      worksheet.addRow(
        Object.assign(
          respondent.toObject(),
          {
            genderGeo: respondent.gender ? respondent.gender.name.ge : '',
            genderEng: respondent.gender ? respondent.gender.name.en : '',
            ageGeo: respondent.ageRange ? respondent.ageRange.title.ge : '',
            ageEng: respondent.ageRange ? respondent.ageRange.title.en : '',
            positionGeo: respondent.position ? respondent.position.name.ge : '',
            positionEng: respondent.position ? respondent.position.name.en : '',
            answerGeo: answer.name.ge,
            answerEng: answer.name.en,
          }
        )
      );
    });
    worksheet.addRow({
      firstName: 'Admin',
      lastName: 'Admin',
      votes: answer.adminVotes,
      answerGeo: answer.name.ge,
      answerEng: answer.name.en,
    });
  });

  return workbook.xlsx.writeFile(filename);
}