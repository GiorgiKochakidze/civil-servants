'use strict';

const _ = require('lodash');
const co = require('co');
const chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
const {expect} = chai;
const testHelpers = require('../../../helpers/testHelpers');
const {ResourceNotFoundError} = require('../../../errors');
const Question = require('../question.dao');
const QuestionStub = require('../../../stubs/question.stub');

describe('question.dao', () => {
  let questionStub;

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();
  }));

  beforeEach(() => {
    questionStub = QuestionStub.getSingle();
  });

  after(testHelpers.clearDB);

  // =============== Getters ===============

  describe('#getAll()', () => {
    it('should get all questions', co.wrap(function* () {
      const questionsData = _.range(10)
        .map(() => QuestionStub.getSingle());

      yield Question.create(questionsData);

      const questions = yield Question.getAll();

      expect(questions).to.be.instanceof(Array);
      expect(questions).to.have.length(questionsData.length);
    }));
  });

  describe('#getByQuery()', () => {
    const TOTAL_COUNT = 24;

    before(co.wrap(function* () {
      yield Question.destroyAll();

      const questions = _.range(TOTAL_COUNT)
        .map(i => QuestionStub.getSingle({
          name: (i % 2 === 0) ? 'value-a' : 'value-b'
        }));

      yield Question.insertMany(questions);
    }));

    it('should get all questions', co.wrap(function* () {
      const {items, numTotal} = yield Question.getByQuery({});
      expect(items).to.have.length(TOTAL_COUNT);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should filter questions by find prop', co.wrap(function* () {
      const find = {name: 'value-a'};
      const {items, numTotal} = yield Question.getByQuery({find});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should filter questions by or prop', co.wrap(function* () {
      const or = [{
        name: {$regex: 'value-a', $options: 'i'}
      }];
      const {items, numTotal} = yield Question.getByQuery({or});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should sort by ascending order', co.wrap(function* () {
      const sort = {name: 1};
      const {items, numTotal} = yield Question.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i].name).to.be.at.least(items[i - 1].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should sort by descending order', co.wrap(function* () {
      const sort = {name: -1};
      const {items, numTotal} = yield Question.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i - 1].name).to.be.at.least(items[i].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get all questions after offset', co.wrap(function* () {
      const offset = 5;
      const {items, numTotal} = yield Question.getByQuery({offset});
      expect(items).to.have.length(TOTAL_COUNT - offset);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get limited number of questions', co.wrap(function* () {
      const limit = 9;
      const {items, numTotal} = yield Question.getByQuery({limit});
      expect(items).to.have.length(limit);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));
  });

  describe('#getById()', () => {
    let question;

    beforeEach(co.wrap(function* () {
      const createdQuestion = yield Question.create(questionStub);
      question = yield Question.getById(createdQuestion._id);
    }));

    it('should get question by id', () => {
      expect(question).to.be.an('object');
    });

    it('should throw error if question was not found', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Question.getById(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Question (id "${dummyId}") was not found`);
    });
  });

  // =============== Setters ===============

  describe('#create()', () => {
    let baseProps;
    let question;

    beforeEach(co.wrap(function* () {
      baseProps = questionStub;
      question = yield Question.create(baseProps);
    }));

    it('should create question', () => {
      expect(question).to.be.an('object');
    });

    it('should contain proper fields', () => {
      expectBasePropsToMatch(question, baseProps);
    });
  });

  describe('#update()', () => {
    it('should update question', co.wrap(function* () {
      const question = yield Question.create(questionStub);

      const updatedProps = {
        name: 'new-my-prop',
      };

      yield Question.update(question._id, updatedProps);

      const updatedQuestion = yield Question.getById(question._id);

      expectBasePropsToMatch(updatedQuestion, updatedProps);
    }));

    it('should throw error if passed question does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      const updatedProps = questionStub;
      return expect(Question.update(dummyId, updatedProps))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not update question (id "${dummyId}")`);
    });
  });

  describe('#destroy()', () => {
    it('should destroy question', co.wrap(function* () {
      const {_id} = yield Question.create(questionStub);
      yield Question.destroy(_id);
      return expect(Question.getById(_id))
        .to.be.rejectedWith(ResourceNotFoundError, `Question (id "${_id}") was not found`);
    }));

    it('should throw error if passed question does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Question.destroy(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not destroy question (id "${dummyId}")`);
    });
  });
});

function expectBasePropsToMatch(actual, expected) {
  expect(actual).to.have.property('name', expected.name);
}
