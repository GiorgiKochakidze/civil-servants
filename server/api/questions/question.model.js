'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;
const multilingual = require('../../schemas/multilingual.schema');

const QuestionSchema = new Schema({
  name: multilingual,
  answers: [{
    name: multilingual,
    adminVotes: { type: Number, default: 0 },
    respondents: [{
      firstName: String,
      lastName: String,
      gender: {type: Schema.Types.ObjectId, ref: 'Gender'},
      ageRange: {type: Schema.Types.ObjectId, ref: 'AgeRange'},
      position: {type: Schema.Types.ObjectId, ref: 'Position'},
      createdAt: { type: Date, default: Date.now }
    }]
  }],
  active: Boolean,
});

module.exports = mongoose.model('Question', QuestionSchema);
