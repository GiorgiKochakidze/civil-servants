'use strict';

const Promise = require('bluebird');
const DBResultHandler = require('../../helpers/DBResultHandler');
const Model = require('./question.model');


module.exports = {
  getAll,
  getByQuery,
  getActiveQuestion,
  getById,

  create,
  insertMany,
  update,
  addAnswer,

  destroy,
  destroyAll
};


// =============== Getters ===============

function getAll() {
  return Model.find();
}

function getActiveQuestion() {
  return Model.findOne({active: true});
}

function getByQuery({find = {}, or = [{}], sort = {_id: -1}, offset, limit}) {
  return Promise.all([
    Model.find(find).or(or).sort(sort).skip(offset).limit(limit),
    Model.find(find).or(or).count()
  ])
  .spread((items, numTotal) => ({items, numTotal}));
}

function getById(id) {
  return Model.findOne({_id: id}).populate('answers.respondents.gender answers.respondents.ageRange answers.respondents.position')
    .then(DBResultHandler.assertFound(`Question (id "${id}") was not found`));
}

// =============== Setters ===============

function create(data) {
  return Model.create(data);
}

function insertMany(data) {
  return Model.insertMany(data);
}

function update(id, data) {
  return Model.findOneAndUpdate({_id: id}, {$set: data})
    .then(DBResultHandler.assertFound(`Could not update question (id "${id}")`));
}

function addAnswer(questionId, answerId, respondent) {
  const findQuery = {'_id': questionId, 'answers._id': answerId};
  const updateQuery = {$push : {'answers.$.respondents': respondent}};
  return Model.findOneAndUpdate(findQuery, updateQuery)
    .then(DBResultHandler.assertFound(`Could not update question (id "${questionId}")`));;
}


function destroy(id) {
  return Model.findOneAndRemove({_id: id})
    .then(DBResultHandler.assertFound(`Could not destroy question (id "${id}")`));
}

function destroyAll() {
  return Model.remove();
}
