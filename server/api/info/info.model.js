'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const multilingual = require('../../schemas/multilingual.schema');

const AboutSchema = {
  projectName: multilingual,
  projectDescription: multilingual,
  statsDescription: multilingual,
  footerDescription: multilingual
};

const InfoSchema = new Schema({
  about: AboutSchema
});

module.exports = mongoose.model('Info', InfoSchema);
