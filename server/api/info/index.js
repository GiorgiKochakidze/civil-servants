'use strict';

const router = require('express').Router();
const co = require('co');
const Info = require('./info.dao');
const parser = require('./info.parser');
const auth = require('../../auth');


module.exports = router;

router.get('/', get);
router.post('/update', auth.isAdmin, parser.parseUpdate, update);

function get(req, res, next) {
  co(function* () {
    const infoData = yield Info.getOne();
    res.json(infoData);
  }).catch(next);
}

function update(req, res, next) {
  co(function* () {
    const parsedInfo = req.parsed;
    yield Info.update(parsedInfo);
    res.sendStatus(200);
  }).catch(next);
}
