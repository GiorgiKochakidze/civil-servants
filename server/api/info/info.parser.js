'use strict';

var _ = require('lodash');


module.exports = {
  parseUpdate,
};

function parseUpdate(req, res, next) {
  req.parsed = _.pick(req.body, ['about']);
  next();
}
