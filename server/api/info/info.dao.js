'use strict';

const DBResultHandler = require('../../helpers/DBResultHandler');
const Model = require('./info.model');


module.exports = {
  getOne,

  create,
  update,

  destroy,
  destroyAll
};

// =============== getters ===============

function getOne() {
  return Model.findOne({})
    .then(DBResultHandler.assertFound('info was not found'));
}

// =============== setters ===============

function create(data) {
  return Model.create(data);
}

function update(data) {
  return Model.findOneAndUpdate({}, { $set: data })
    .then(DBResultHandler.assertFound('could not update info'));
}

function destroy() {
  return Model.findOneAndRemove({})
    .then(DBResultHandler.assertFound(`could not destroy info`));
}

function destroyAll() {
  return Model.remove();
}
