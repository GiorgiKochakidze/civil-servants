'use strict';

const router = require('express').Router();
const co = require('co');
const config = require('../../config/environment');
const User = require('./user.dao');
const parser = require('./user.parser');
const validator = require('./user.validator');
const auth = require('../../auth');
const encryption = require('../../helpers/encryption');


module.exports = router;

router.get('/me', getMe);
router.post('/sign-in/admin', parser.parseSignInAdmin, validator.validateSignInAdmin, signInAdmin);
router.post('/password/update', auth.isAdmin, parser.parseUpdatePassword, validator.validateUpdatePassword, update);

function getMe(req, res) {
  res.json(req.user);
}

function update(req, res, next) {
  co(function* () {
    const {_id} = req.user;
    const payload = req.parsed;
    if (payload.newPassword) {
      payload.passwordHash = yield encryption.generateHash(payload.newPassword);
    }
    yield User.update(_id, payload);
    res.sendStatus(200);
  })
  .catch(next);
}

function signInAdmin(req, res, next) {
  co(function* () {
    const {email} = req.parsed;
    const user = yield User.getByEmail(email);
    const token = auth.signToken(user);
    res.cookie('token', token, config.cookie);
    res.sendStatus(200);
  })
  .catch(next);
}
