'use strict';

const DBResultHandler = require('../../helpers/DBResultHandler');
const ResourceNotFoundError = require('../../errors').ResourceNotFoundError;
const Model = require('./user.model');

const PRIVATE_FIELDS = '-passwordHash';


module.exports = {
  getById,
  getByEmail,
  getByEmailConfirmationToken,
  getPasswordHashByEmail,

  create,
  update,

  destroy,
  destroyAll
};


// =============== Getters ===============

function getById(_id) {
  return Model.findById({_id}, PRIVATE_FIELDS)
    .then(DBResultHandler.assertFound(`user (id "${_id}") was not found`));
}

function getByEmail(email) {
  return Model.findOne({email}, PRIVATE_FIELDS)
    .then(DBResultHandler.assertFound(`user (email "${email}") was not found`));
}

function getByEmailConfirmationToken(emailConfirmationToken) {
  return Model.findOne({emailConfirmationToken}, PRIVATE_FIELDS)
    .then(DBResultHandler.assertFound(`user (email confirmation token "${emailConfirmationToken}") was not found`));
}

function getPasswordHashByEmail(email) {
  return Model.findOne({email}, 'passwordHash')
    .then(res => {
      if (res === null) {
        throw new ResourceNotFoundError(`user (email "${email}") was not found`);
      } else if (!res.passwordHash) {
        throw new ResourceNotFoundError(`user (email "${email}") does not have a passwordHash`);
      } else {
        return res.passwordHash;
      }
    });
}


// =============== Setters ===============

function create(data) {
  return Model.create(data);
}

function update(id, data) {
  return Model.findOneAndUpdate({_id: id}, {$set: data})
    .then(DBResultHandler.assertFound(`could not update user (id "${id}")`));
}

function destroy(id) {
  return Model.findOneAndRemove({_id: id})
    .then(DBResultHandler.assertFound(`could not destroy user (id "${id}")`));
}

function destroyAll() {
  return Model.remove();
}
