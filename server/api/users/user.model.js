'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
  email: String,
  fullName: String,

  passwordHash: String,

  role: String,

  joinedAt: Date,
  isEmailConfirmed: Boolean,
  emailConfirmationToken: String,
});

UserSchema.index({email: 1}, {unique: true});

module.exports = mongoose.model('User', UserSchema);
