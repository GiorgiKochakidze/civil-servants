'use strict';

const Promise = require('bluebird');
const request = require('supertest');
const app = require('../../../../server');
const User = require('../../user.dao');
const agent = request.agent(app);


module.exports = {
  build
};


function build(user) {
  let token;

  return {
    authorize,
    get: (url) => agent.get(url).set('Cookie', `token=${token}`),
    post: (url) => agent.post(url).set('Cookie', `token=${token}`),
    delete: (url) => agent.delete(url).set('Cookie', `token=${token}`)
  };

  function authorize() {
    return new Promise((resolve, reject) => {
      User.create(user)
        .then(() => {
          agent
            .post('/api/users/sign-in')
            .send(user)
            .expect((res) => {
              token = parseTokenFromHeader(res.header['set-cookie']);
            })
            .end((err, res) => {
              if (err) {
                reject(err);
              } else {
                resolve(res);
              }
            });
        });
    });
  }
}

function parseTokenFromHeader(header) {
  const pair = header.filter(a => a.startsWith('token='))[0];
  return pair.substr(pair.indexOf('=') + 1);
}
