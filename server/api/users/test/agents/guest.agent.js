'use strict';

const builder = require('./builder');


module.exports = () => {
  return builder.build({});
};
