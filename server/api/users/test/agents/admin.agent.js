'use strict';

const UserStub = require('../../../../stubs/user.stub');
const roles = require('../../../../constants/user').roles;
const builder = require('./builder');


module.exports = () => {
  const admin = UserStub.getSingle({
    password: 'password',
    role: roles.ADMIN
  });
  return builder.build(admin);
};
