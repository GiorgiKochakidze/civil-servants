'use strict';

const getGuestAgent = require('./guest.agent');
const getUserAgent = require('./user.agent');
const getAdminAgent = require('./admin.agent');


module.exports = {
  getGuestAgent,
  getUserAgent,
  getAdminAgent
};
