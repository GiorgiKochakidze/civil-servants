'use strict';

const UserStub = require('../../../../stubs/user.stub');
const roles = require('../../../../constants/user').roles;
const builder = require('./builder');


module.exports = () => {
  const user = UserStub.getSingle({
    password: 'password',
    role: roles.USER
  });
  return builder.build(user);
};
