'use strict';

const co = require('co');
const testHelpers = require('../../../helpers/testHelpers');
const agents = require('../../users/test/agents');
const User = require('../user.dao');
const UserStub = require('../../../stubs/user.stub');


describe('api/users', () => {
  const guestAgent = agents.getGuestAgent();
  const userAgent = agents.getUserAgent();
  const adminAgent = agents.getAdminAgent();

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();

    yield userAgent.authorize();
    yield adminAgent.authorize();
  }));

  after(testHelpers.clearDB);

  // =============== GET ===============

  describe('GET /me', () => {
    const route = '/api/users/me';

    it('should return 200 for guest', (done) => {
      guestAgent.get(route).expect(200, done);
    });

    it('should return 200 for user', (done) => {
      userAgent.get(route).expect(200, done);
    });

    it('should return 200 for admin', (done) => {
      adminAgent.get(route).expect(200, done);
    });
  });

  // =============== POST ===============

  describe('POST /', () => {
    const route = '/api/users';
    let userStub;

    beforeEach(() => {
      userStub = UserStub.getSingle();
    });

    it('should return 201 for guest', (done) => {
      guestAgent.post(route).send(userStub).expect(201, done);
    });

    it('should return 201 for user', (done) => {
      userAgent.post(route).send(userStub).expect(201, done);
    });

    it('should return 201 for admin', (done) => {
      adminAgent.post(route).send(userStub).expect(201, done);
    });
  });

  describe('POST /update', () => {
    const route = '/api/users/update';

    it('should return 401 for guest', (done) => {
      guestAgent.post(route).send({}).expect(401, done);
    });

    it('should return 200 for user', (done) => {
      userAgent.post(route).send({}).expect(200, done);
    });

    it('should return 200 for admin', (done) => {
      adminAgent.post(route).send({}).expect(200, done);
    });
  });

  describe('DELETE /', () => {
    let route;

    beforeEach(co.wrap(function* () {
      const user = yield User.create(UserStub.getSingle());
      route = `/api/users/${user._id}`;
    }));

    it('should return 401 for guest', (done) => {
      guestAgent.delete(route).expect(401, done);
    });

    it('should return 401 for user', (done) => {
      userAgent.delete(route).expect(401, done);
    });

    it('should return 200 for admin', (done) => {
      adminAgent.delete(route).expect(200, done);
    });
  });
});
