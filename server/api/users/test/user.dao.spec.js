'use strict';

const co = require('co');
const chai = require('chai');
const {expect} = chai;
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
const testHelpers = require('../../../helpers/testHelpers');
const {ResourceNotFoundError} = require('../../../errors');
const User = require('../user.dao');
const UserStub = require('../../../stubs/user.stub');


describe('user.dao', () => {
  let userStub;

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();
  }));

  beforeEach(() => {
    userStub = UserStub.getSingle();
  });

  after(testHelpers.clearDB);


  // =============== getters ===============

  describe('#getById()', () => {
    let user;

    beforeEach(co.wrap(function* () {
      const createdUser = yield User.create(userStub);
      user = yield User.getById(createdUser._id);
    }));

    it('should get user by id', () => {
      expect(user).to.be.an('object');
    });

    it('should not contain password hash', () => {
      expect(user.passwordHash).to.not.exist;
    });

    it('should throw error if user was not found', () => {
      const dummyId = testHelpers.DUMMY_ID;

      return expect(User.getById(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `user (id "${dummyId}") was not found`);
    });
  });

  describe('#getByEmail()', () => {
    let user;

    beforeEach(co.wrap(function* () {
      yield User.create(userStub);
      user = yield User.getByEmail(userStub.email);
    }));

    it('should get user by email', () => {
      expect(user).to.be.an('object');
    });

    it('should not contain password hash', () => {
      expect(user.passwordHash).to.not.exist;
    });

    it('should throw error if user was not found', () => {
      const dummyEmail = testHelpers.DUMMY_EMAIL;

      return expect(User.getByEmail(dummyEmail))
        .to.be.rejectedWith(ResourceNotFoundError, `user (email "${dummyEmail}") was not found`);
    });
  });

  describe('#getPasswordHashByEmail()', () => {
    it('should get passwordHash', co.wrap(function* () {
      const user = yield User.create(userStub);
      const passwordHash = yield User.getPasswordHashByEmail(user.email);
      expect(passwordHash).to.equal(user.passwordHash);
    }));

    it('should throw error if user does not have passwordHash', co.wrap(function* () {
      delete userStub.passwordHash;
      const user = yield User.create(userStub);

      return expect(User.getPasswordHashByEmail(user.email))
        .to.be.rejectedWith(ResourceNotFoundError, `user (email "${user.email}") does not have a passwordHash`);
    }));

    it('should throw error if user was not found', () => {
      const dummyEmail = testHelpers.DUMMY_EMAIL;

      return expect(User.getPasswordHashByEmail(dummyEmail))
        .to.be.rejectedWith(ResourceNotFoundError, `user (email "${dummyEmail}") was not found`);
    });
  });


  // =============== setters ===============

  describe('#create()', () => {
    let userData;
    let user;

    beforeEach(co.wrap(function* () {
      userData = userStub;
      user = yield User.create(userData);
    }));

    it('should create user', () => {
      expect(user).to.be.an('object');
    });

    it('should contain proper fields', () => {
      expectBaseFieldsToMatch(user, userData);
    });
  });

  describe('#update()', () => {
    it('should update user', co.wrap(function* () {
      const user = yield User.create(userStub);

      const newData = {
        email: 'new-email',
        fullName: 'new-full-name',
        passwordHash: 'new-password',
        role: 'admin',
        isEmailConfirmed: false,
        emailConfirmationToken: 'new-emailConfirmationToken',
        joinedAt: '2016-02-17T07:13:41.789Z'
      };

      yield User.update(user._id, newData);

      const updatedUser = yield User.getById(user._id);
      updatedUser.passwordHash = yield User.getPasswordHashByEmail(newData.email);

      expectBaseFieldsToMatch(updatedUser, newData);
    }));

    it('should throw error if passed user does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      const newData = userStub;

      return expect(User.update(dummyId, newData))
        .to.be.rejectedWith(ResourceNotFoundError, `could not update user (id "${dummyId}")`);
    });
  });

  describe('#destroy()', () => {
    it('should destroy user', co.wrap(function* () {
      const user = yield User.create(userStub);

      yield User.destroy(user._id);

      return expect(User.getById(user._id))
        .to.be.rejectedWith(ResourceNotFoundError, `user (id "${user._id}") was not found`);
    }));

    it('should throw error if passed user does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;

      return expect(User.destroy(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `could not destroy user (id "${dummyId}")`);
    });
  });
});

function expectBaseFieldsToMatch(actual, expected) {
  expect(actual).to.have.property('email', expected.email);
  expect(actual).to.have.property('fullName', expected.fullName);
  expect(actual).to.have.property('passwordHash', expected.passwordHash);
  expect(actual).to.have.property('role', expected.role);
  expect(actual.joinedAt).to.equalDate(new Date(expected.joinedAt));
  expect(actual).to.have.property('isEmailConfirmed', expected.isEmailConfirmed);
  expect(actual).to.have.property('emailConfirmationToken', expected.emailConfirmationToken);
}
