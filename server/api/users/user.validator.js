'use strict';

const co = require('co');
const User = require('./user.dao');
const encryption = require('../../helpers/encryption');
const errors = require('../../errors');
const ResourceNotFoundError = errors.ResourceNotFoundError;
const ValidationError = errors.ValidationError;


module.exports = {
  validateUniqueEmail,

  validateCreate,
  validateUpdate,
  validateUpdatePassword,

  validateSignInAdmin,
};


function validateUniqueEmail(req, res, next) {
  co(function* () {
    const {email} = req.params;
    yield checkUniqueEmail(email);
    next();
  })
  .catch(next);
}

function validateCreate(req, res, next) {
  co(function* () {
    const {email} = req.parsed;
    yield checkUniqueEmail(email);
    next();
  })
  .catch(next);
}

function validateUpdate(req, res, next) {
  co(function* () {
    const {_id} = req.user;
    const {email} = req.parsed;
    const {email: curEmail} = yield User.getById(_id);
    if (email !== curEmail) {
      yield checkUniqueEmail(email);
    }
    next();
  })
  .catch(next);
}

function validateUpdatePassword(req, res, next) {
  co(function* () {
    const {email} = req.user;
    const {currentPassword} = req.parsed;
    yield checkPassword(email, currentPassword);
    next();
  })
  .catch(next);
}

function validateSignInAdmin(req, res, next) {
  co(function* () {
    const {email, password} = req.parsed;
    yield checkPassword(email, password);
    next();
  })
  .catch(next);
}

// =============== Helpers ===============

function checkUniqueEmail(email) {
  return User.getByEmail(email)
    .then(() => {
      throw new ValidationError(`Email (${email}) is not unique`);
    })
    .catch(e => {
      if (!(e instanceof ResourceNotFoundError)) {
        throw e;
      }
    });
}

function checkPassword(email, password) {
  return co(function* () {
    const passwordHash = yield User.getPasswordHashByEmail(email);
    const isValidPassword = yield encryption.compareHash(password, passwordHash);

    if (!isValidPassword) {
      throw new ValidationError(`Password of user (email ${email}) is invalid`);
    }
  });
}
