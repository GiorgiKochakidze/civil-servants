'use strict';

const _ = require('lodash');


module.exports = {
  parseCreate,
  parseUpdatePassword,
  parseSignInAdmin
};


// =============== POST ===============

function parseCreate(req, res, next) {
  req.parsed = Object.assign(
    parseBaseProps(req.body),
    {
      password: req.body.password
    }
  );
  next();
}

function parseUpdatePassword(req, res, next) {
  req.parsed = _.pick(req.body, ['currentPassword', 'newPassword']);
  next();
}

function parseSignInAdmin(req, res, next) {
  req.parsed = _.pick(req.body, ['email', 'password']);
  next();
}

function parseBaseProps(body) {
  return _.pick(body, ['email', 'fullName']);
}
