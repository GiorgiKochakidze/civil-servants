'use strict';

const fs = require('fs');
const path = require('path');
const router = require('express').Router();
const multer = require('multer');
const upload = multer(require('../../config/multer'));
const parser = require('./file.parser');
const config = require('../../config/environment');


module.exports = router;


router.post('/', upload.array('filesToAdd', 16), parser.parseCreateAndDestroy, destroy);

function destroy(req, res) {
  const filesToDestroy = req.parsed.filesToDestroy;
  for (const filename of filesToDestroy) {
    const filepath = path.join(config.paths.uploads, filename || '');
    fs.unlink(filepath, () => {});
  }
  res.sendStatus(200);
}
