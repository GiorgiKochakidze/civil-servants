'use strict';

const utils = require('../../helpers/parserUtils');


module.exports = {
  parseCreateAndDestroy
};


function parseCreateAndDestroy(req, res, next) {
  req.parsed = {
    filesToDestroy: utils.parseArray(req.body.filesToDestroy)
  };
  next();
}
