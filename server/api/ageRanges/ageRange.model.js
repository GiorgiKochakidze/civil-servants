'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;
const multilingual = require('../../schemas/multilingual.schema');


const AgeRangeSchema = new Schema({
  title: multilingual,
  from: Number,
  to: Number,
});

module.exports = mongoose.model('AgeRange', AgeRangeSchema);
