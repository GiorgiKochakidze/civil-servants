'use strict';

const _ = require('lodash');
const co = require('co');
const chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
const {expect} = chai;
const testHelpers = require('../../../helpers/testHelpers');
const {ResourceNotFoundError} = require('../../../errors');
const AgeRange = require('../ageRange.dao');
const AgeRangeStub = require('../../../stubs/ageRange.stub');

describe('ageRange.dao', () => {
  let ageRangeStub;

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();
  }));

  beforeEach(() => {
    ageRangeStub = AgeRangeStub.getSingle();
  });

  after(testHelpers.clearDB);

  // =============== Getters ===============

  describe('#getAll()', () => {
    it('should get all ageRanges', co.wrap(function* () {
      const ageRangesData = _.range(10)
        .map(() => AgeRangeStub.getSingle());

      yield AgeRange.create(ageRangesData);

      const ageRanges = yield AgeRange.getAll();

      expect(ageRanges).to.be.instanceof(Array);
      expect(ageRanges).to.have.length(ageRangesData.length);
    }));
  });

  describe('#getByQuery()', () => {
    const TOTAL_COUNT = 24;

    before(co.wrap(function* () {
      yield AgeRange.destroyAll();

      const ageRanges = _.range(TOTAL_COUNT)
        .map(i => AgeRangeStub.getSingle({
          title: (i % 2 === 0) ? 'value-a' : 'value-b'
        }));

      yield AgeRange.insertMany(ageRanges);
    }));

    it('should get all ageRanges', co.wrap(function* () {
      const {items, numTotal} = yield AgeRange.getByQuery({});
      expect(items).to.have.length(TOTAL_COUNT);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should filter ageRanges by find prop', co.wrap(function* () {
      const find = {title: 'value-a'};
      const {items, numTotal} = yield AgeRange.getByQuery({find});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should filter ageRanges by or prop', co.wrap(function* () {
      const or = [{
        title: {$regex: 'value-a', $options: 'i'}
      }];
      const {items, numTotal} = yield AgeRange.getByQuery({or});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should sort by ascending order', co.wrap(function* () {
      const sort = {title: 1};
      const {items, numTotal} = yield AgeRange.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i].title).to.be.at.least(items[i - 1].title);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should sort by descending order', co.wrap(function* () {
      const sort = {title: -1};
      const {items, numTotal} = yield AgeRange.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i - 1].title).to.be.at.least(items[i].title);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get all ageRanges after offset', co.wrap(function* () {
      const offset = 5;
      const {items, numTotal} = yield AgeRange.getByQuery({offset});
      expect(items).to.have.length(TOTAL_COUNT - offset);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get limited number of ageRanges', co.wrap(function* () {
      const limit = 9;
      const {items, numTotal} = yield AgeRange.getByQuery({limit});
      expect(items).to.have.length(limit);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));
  });

  describe('#getById()', () => {
    let ageRange;

    beforeEach(co.wrap(function* () {
      const createdAgeRange = yield AgeRange.create(ageRangeStub);
      ageRange = yield AgeRange.getById(createdAgeRange._id);
    }));

    it('should get ageRange by id', () => {
      expect(ageRange).to.be.an('object');
    });

    it('should throw error if ageRange was not found', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(AgeRange.getById(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `AgeRange (id "${dummyId}") was not found`);
    });
  });

  // =============== Setters ===============

  describe('#create()', () => {
    let baseProps;
    let ageRange;

    beforeEach(co.wrap(function* () {
      baseProps = ageRangeStub;
      ageRange = yield AgeRange.create(baseProps);
    }));

    it('should create ageRange', () => {
      expect(ageRange).to.be.an('object');
    });

    it('should contain proper fields', () => {
      expectBasePropsToMatch(ageRange, baseProps);
    });
  });

  describe('#update()', () => {
    it('should update ageRange', co.wrap(function* () {
      const ageRange = yield AgeRange.create(ageRangeStub);

      const updatedProps = {
        title: 'new-my-prop',
      };

      yield AgeRange.update(ageRange._id, updatedProps);

      const updatedAgeRange = yield AgeRange.getById(ageRange._id);

      expectBasePropsToMatch(updatedAgeRange, updatedProps);
    }));

    it('should throw error if passed ageRange does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      const updatedProps = ageRangeStub;
      return expect(AgeRange.update(dummyId, updatedProps))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not update ageRange (id "${dummyId}")`);
    });
  });

  describe('#destroy()', () => {
    it('should destroy ageRange', co.wrap(function* () {
      const {_id} = yield AgeRange.create(ageRangeStub);
      yield AgeRange.destroy(_id);
      return expect(AgeRange.getById(_id))
        .to.be.rejectedWith(ResourceNotFoundError, `AgeRange (id "${_id}") was not found`);
    }));

    it('should throw error if passed ageRange does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(AgeRange.destroy(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not destroy ageRange (id "${dummyId}")`);
    });
  });
});

function expectBasePropsToMatch(actual, expected) {
  expect(actual).to.have.property('title', expected.title);
}
