'use strict';

const router = require('express').Router();
const co = require('co');
const AgeRange = require('./ageRange.dao');
const parser = require('./ageRange.parser');
const auth = require('../../auth');


module.exports = router;


router.get('/', parser.parseGetByQuery, getByQuery);

router.post('/', auth.isAdmin, parser.parseCreate, create);
router.post('/update', auth.isAdmin, parser.parseUpdate, update);

router.delete('/:ageRangeId', auth.isAdmin, destroy);


// =============== GET ===============

function getByQuery(req, res, next) {
  co(function* () {
    const query = req.parsed;
    const ageRangesData = yield AgeRange.getByQuery(query);
    res.json(ageRangesData);
  })
  .catch(next);
}

// =============== POST ===============

function create(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield AgeRange.create(payload);
    res.sendStatus(201);
  })
  .catch(next);
}

function update(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield AgeRange.update(payload._id, payload);
    res.sendStatus(200);
  })
  .catch(next);
}

function destroy(req, res, next) {
  co(function* () {
    const {ageRangeId} = req.params;
    yield AgeRange.destroy(ageRangeId);
    res.sendStatus(200);
  })
  .catch(next);
}
