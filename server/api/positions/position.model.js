'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;
const multilingual = require('../../schemas/multilingual.schema');

const PositionSchema = new Schema({
  name: multilingual,
});


module.exports = mongoose.model('Position', PositionSchema);
