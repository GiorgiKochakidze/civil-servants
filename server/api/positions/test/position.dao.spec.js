'use strict';

const _ = require('lodash');
const co = require('co');
const chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
const {expect} = chai;
const testHelpers = require('../../../helpers/testHelpers');
const {ResourceNotFoundError} = require('../../../errors');
const Position = require('../position.dao');
const PositionStub = require('../../../stubs/position.stub');

describe('position.dao', () => {
  let positionStub;

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();
  }));

  beforeEach(() => {
    positionStub = PositionStub.getSingle();
  });

  after(testHelpers.clearDB);

  // =============== Getters ===============

  describe('#getAll()', () => {
    it('should get all positions', co.wrap(function* () {
      const positionsData = _.range(10)
        .map(() => PositionStub.getSingle());

      yield Position.create(positionsData);

      const positions = yield Position.getAll();

      expect(positions).to.be.instanceof(Array);
      expect(positions).to.have.length(positionsData.length);
    }));
  });

  describe('#getByQuery()', () => {
    const TOTAL_COUNT = 24;

    before(co.wrap(function* () {
      yield Position.destroyAll();

      const positions = _.range(TOTAL_COUNT)
        .map(i => PositionStub.getSingle({
          name: (i % 2 === 0) ? 'value-a' : 'value-b'
        }));

      yield Position.insertMany(positions);
    }));

    it('should get all positions', co.wrap(function* () {
      const {items, numTotal} = yield Position.getByQuery({});
      expect(items).to.have.length(TOTAL_COUNT);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should filter positions by find prop', co.wrap(function* () {
      const find = {name: 'value-a'};
      const {items, numTotal} = yield Position.getByQuery({find});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should filter positions by or prop', co.wrap(function* () {
      const or = [{
        name: {$regex: 'value-a', $options: 'i'}
      }];
      const {items, numTotal} = yield Position.getByQuery({or});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should sort by ascending order', co.wrap(function* () {
      const sort = {name: 1};
      const {items, numTotal} = yield Position.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i].name).to.be.at.least(items[i - 1].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should sort by descending order', co.wrap(function* () {
      const sort = {name: -1};
      const {items, numTotal} = yield Position.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i - 1].name).to.be.at.least(items[i].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get all positions after offset', co.wrap(function* () {
      const offset = 5;
      const {items, numTotal} = yield Position.getByQuery({offset});
      expect(items).to.have.length(TOTAL_COUNT - offset);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get limited number of positions', co.wrap(function* () {
      const limit = 9;
      const {items, numTotal} = yield Position.getByQuery({limit});
      expect(items).to.have.length(limit);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));
  });

  describe('#getById()', () => {
    let position;

    beforeEach(co.wrap(function* () {
      const createdPosition = yield Position.create(positionStub);
      position = yield Position.getById(createdPosition._id);
    }));

    it('should get position by id', () => {
      expect(position).to.be.an('object');
    });

    it('should throw error if position was not found', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Position.getById(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Position (id ${dummyId}) was not found`);
    });
  });

  // =============== Setters ===============

  describe('#create()', () => {
    let baseProps;
    let position;

    beforeEach(co.wrap(function* () {
      baseProps = positionStub;
      position = yield Position.create(baseProps);
    }));

    it('should create position', () => {
      expect(position).to.be.an('object');
    });

    it('should contain proper fields', () => {
      expectBasePropsToMatch(position, baseProps);
    });
  });

  describe('#update()', () => {
    it('should update position', co.wrap(function* () {
      const {_id} = yield Position.create(positionStub);

      const updatedProps = {
        name: 'new-my-prop',
      };

      yield Position.update(_id, updatedProps);

      const updatedPosition = yield Position.getById(_id);

      expectBasePropsToMatch(updatedPosition, updatedProps);
    }));

    it('should throw error if passed position does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      const updatedProps = positionStub;
      return expect(Position.update(dummyId, updatedProps))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not update position (id ${dummyId})`);
    });
  });

  describe('#destroy()', () => {
    it('should destroy position', co.wrap(function* () {
      const {_id} = yield Position.create(positionStub);
      yield Position.destroy(_id);
      return expect(Position.getById(_id))
        .to.be.rejectedWith(ResourceNotFoundError, `Position (id ${_id}) was not found`);
    }));

    it('should throw error if passed position does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Position.destroy(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not destroy position (id ${dummyId})`);
    });
  });
});

function expectBasePropsToMatch(actual, expected) {
  expect(actual).to.have.property('name', expected.name);
}
