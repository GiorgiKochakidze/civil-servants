'use strict';

const router = require('express').Router();
const co = require('co');
const Gender = require('./gender.dao');
const parser = require('./gender.parser');
const auth = require('../../auth');


module.exports = router;


router.get('/', parser.parseGetByQuery, getByQuery);

router.post('/', auth.isAdmin, parser.parseCreate, create);
router.post('/update', auth.isAdmin, parser.parseUpdate, update);

router.delete('/:genderId', auth.isAdmin, destroy);


// =============== GET ===============

function getByQuery(req, res, next) {
  co(function* () {
    const query = req.parsed;
    const gendersData = yield Gender.getByQuery(query);
    res.json(gendersData);
  })
  .catch(next);
}

// =============== POST ===============

function create(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield Gender.create(payload);
    res.sendStatus(201);
  })
  .catch(next);
}

function update(req, res, next) {
  co(function* () {
    const payload = req.parsed;
    yield Gender.update(payload._id, payload);
    res.sendStatus(200);
  })
  .catch(next);
}

function destroy(req, res, next) {
  co(function* () {
    const {genderId} = req.params;
    yield Gender.destroy(genderId);
    res.sendStatus(200);
  })
  .catch(next);
}
