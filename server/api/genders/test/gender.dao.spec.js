'use strict';

const _ = require('lodash');
const co = require('co');
const chai = require('chai');
chai.use(require('chai-as-promised'));
chai.use(require('chai-datetime'));
const {expect} = chai;
const testHelpers = require('../../../helpers/testHelpers');
const {ResourceNotFoundError} = require('../../../errors');
const Gender = require('../gender.dao');
const GenderStub = require('../../../stubs/gender.stub');

describe('gender.dao', () => {
  let genderStub;

  before(co.wrap(function* () {
    testHelpers.connectDB();
    yield testHelpers.clearDB();
  }));

  beforeEach(() => {
    genderStub = GenderStub.getSingle();
  });

  after(testHelpers.clearDB);

  // =============== Getters ===============

  describe('#getAll()', () => {
    it('should get all genders', co.wrap(function* () {
      const gendersData = _.range(10)
        .map(() => GenderStub.getSingle());

      yield Gender.create(gendersData);

      const genders = yield Gender.getAll();

      expect(genders).to.be.instanceof(Array);
      expect(genders).to.have.length(gendersData.length);
    }));
  });

  describe('#getByQuery()', () => {
    const TOTAL_COUNT = 24;

    before(co.wrap(function* () {
      yield Gender.destroyAll();

      const genders = _.range(TOTAL_COUNT)
        .map(i => GenderStub.getSingle({
          name: (i % 2 === 0) ? 'value-a' : 'value-b'
        }));

      yield Gender.insertMany(genders);
    }));

    it('should get all genders', co.wrap(function* () {
      const {items, numTotal} = yield Gender.getByQuery({});
      expect(items).to.have.length(TOTAL_COUNT);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should filter genders by find prop', co.wrap(function* () {
      const find = {name: 'value-a'};
      const {items, numTotal} = yield Gender.getByQuery({find});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should filter genders by or prop', co.wrap(function* () {
      const or = [{
        name: {$regex: 'value-a', $options: 'i'}
      }];
      const {items, numTotal} = yield Gender.getByQuery({or});
      expect(items).to.have.length(TOTAL_COUNT / 2);
      expect(numTotal).to.equal(TOTAL_COUNT / 2);
    }));

    it('should sort by ascending order', co.wrap(function* () {
      const sort = {name: 1};
      const {items, numTotal} = yield Gender.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i].name).to.be.at.least(items[i - 1].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should sort by descending order', co.wrap(function* () {
      const sort = {name: -1};
      const {items, numTotal} = yield Gender.getByQuery({sort});
      expect(items).to.have.length(TOTAL_COUNT);
      for (let i = 1; i < items.length; i++) {
        expect(items[i - 1].name).to.be.at.least(items[i].name);
      }
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get all genders after offset', co.wrap(function* () {
      const offset = 5;
      const {items, numTotal} = yield Gender.getByQuery({offset});
      expect(items).to.have.length(TOTAL_COUNT - offset);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));

    it('should get limited number of genders', co.wrap(function* () {
      const limit = 9;
      const {items, numTotal} = yield Gender.getByQuery({limit});
      expect(items).to.have.length(limit);
      expect(numTotal).to.equal(TOTAL_COUNT);
    }));
  });

  describe('#getById()', () => {
    let gender;

    beforeEach(co.wrap(function* () {
      const createdGender = yield Gender.create(genderStub);
      gender = yield Gender.getById(createdGender._id);
    }));

    it('should get gender by id', () => {
      expect(gender).to.be.an('object');
    });

    it('should throw error if gender was not found', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Gender.getById(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Gender (id "${dummyId}") was not found`);
    });
  });

  // =============== Setters ===============

  describe('#create()', () => {
    let baseProps;
    let gender;

    beforeEach(co.wrap(function* () {
      baseProps = genderStub;
      gender = yield Gender.create(baseProps);
    }));

    it('should create gender', () => {
      expect(gender).to.be.an('object');
    });

    it('should contain proper fields', () => {
      expectBasePropsToMatch(gender, baseProps);
    });
  });

  describe('#update()', () => {
    it('should update gender', co.wrap(function* () {
      const gender = yield Gender.create(genderStub);

      const updatedProps = {
        name: 'new-my-prop',
      };

      yield Gender.update(gender._id, updatedProps);

      const updatedGender = yield Gender.getById(gender._id);

      expectBasePropsToMatch(updatedGender, updatedProps);
    }));

    it('should throw error if passed gender does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      const updatedProps = genderStub;
      return expect(Gender.update(dummyId, updatedProps))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not update gender (id "${dummyId}")`);
    });
  });

  describe('#destroy()', () => {
    it('should destroy gender', co.wrap(function* () {
      const {_id} = yield Gender.create(genderStub);
      yield Gender.destroy(_id);
      return expect(Gender.getById(_id))
        .to.be.rejectedWith(ResourceNotFoundError, `Gender (id "${_id}") was not found`);
    }));

    it('should throw error if passed gender does not exist', () => {
      const dummyId = testHelpers.DUMMY_ID;
      return expect(Gender.destroy(dummyId))
        .to.be.rejectedWith(ResourceNotFoundError, `Could not destroy gender (id "${dummyId}")`);
    });
  });
});

function expectBasePropsToMatch(actual, expected) {
  expect(actual).to.have.property('name', expected.name);
}
