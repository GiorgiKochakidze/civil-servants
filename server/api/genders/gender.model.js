'use strict';

const mongoose = require('mongoose');
const {Schema} = mongoose;
const multilingual = require('../../schemas/multilingual.schema');

const GenderSchema = new Schema({
  name: multilingual
});

module.exports = mongoose.model('Gender', GenderSchema);
