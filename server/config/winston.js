'use strict';

const fs = require('fs');
const path = require('path');
const winston = require('winston');
const config = require('../config/environment');

if (!fs.existsSync(config.paths.root)) {
  fs.mkdirSync(config.paths.root);
}

if (!fs.existsSync(config.paths.log)) {
  fs.mkdirSync(config.paths.log);
}

const filename = path.join(config.paths.log, `${config.env}.log`);

winston.add(winston.transports.File, {filename});
