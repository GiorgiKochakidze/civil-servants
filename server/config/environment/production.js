'use strict';

const pkg = require('../../../package.json');

module.exports = {
  port: pkg.config.productionPort,

  url: {
    scheme: pkg.config.scheme,
    host: pkg.config.host
  },

  mongo: {
    uri: `mongodb://localhost/${pkg.name}`,
    options: {}
  },

  // seedDB: true
};
