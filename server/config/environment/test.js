'use strict';

const pkg = require('../../../package.json');

module.exports = {
  port: 9001,

  url: {
    scheme: 'http',
    host: `localhost:${pkg.config.developmentPort}`
  },

  mongo: {
    uri: `mongodb://localhost/${pkg.name}-test`,
    options: {}
  }
};
