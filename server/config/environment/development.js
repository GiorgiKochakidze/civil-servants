'use strict';

const pkg = require('../../../package.json');

module.exports = {
  port: pkg.config.developmentPort,

  url: {
    scheme: 'http',
    host: `localhost:${pkg.config.liveReloadPort}`
  },

  mongo: {
    uri: `mongodb://localhost/${pkg.name}-dev`,
    options: {}
  },

  seedDB: true
};
