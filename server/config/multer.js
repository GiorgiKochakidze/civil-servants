'use strict';

const multer = require('multer');
const config = require('./environment');

const storage = multer.diskStorage({
  destination: config.paths.uploads,

  filename: (req, file, cb) => {
    cb(null, file.originalname);
  }
});


module.exports = {
  storage
};
