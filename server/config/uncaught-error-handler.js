'use strict';

const config = require('../config/environment');
const mailer = require('../helpers/mailer');
const logger = require('../helpers/logger');


process.on('uncaughtException', handleError);
process.on('unhandledRejection', handleError);


function handleError(err) {
  const text = 'Uncaught Error! - ' + JSON.stringify(err.stack || err);

  logger.error(text);

  if (config.env === 'production') {
    const subject = config.url.host + ' - Uncaught Exception';
    mailer.send('lucymailertest@gmail.com', subject, text);
  } else {
    process.exit(1);
  }
}
