'use strict';

const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const config = require('./environment');
const logger = require('../helpers/logger');

mongoose.connect(config.mongo.uri, config.mongo.options);

logger.info(`${config.mongo.uri} connected to mongoose`);
