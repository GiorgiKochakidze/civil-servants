'use strict';

const co = require('co');
const Promise = require('bluebird');
const logger = require('../helpers/logger');
const {roles} = require('../constants/user');

//inject:require.daos
const AgeRange = require('../api/ageRanges/ageRange.dao');
const File = require('../api/files/file.dao');
const Gender = require('../api/genders/gender.dao');
const Info = require('../api/info/info.dao');
const Position = require('../api/positions/position.dao');
const Question = require('../api/questions/question.dao');
const User = require('../api/users/user.dao');
//endinject


//inject:require.stubs
const AgeRangeStub = require('../stubs/ageRange.stub');
const FileStub = require('../stubs/file.stub');
const GenderStub = require('../stubs/gender.stub');
const InfoStub = require('../stubs/info.stub');
const PositionStub = require('../stubs/position.stub');
const QuestionStub = require('../stubs/question.stub');
const UserStub = require('../stubs/user.stub');
//endinject


module.exports = {
  seedDB,
  seedDBProduction,
  clearDB
};

function seedDB() {
  return co(function* () {
    yield clearDB();
    yield User.create(UserStub.getSingle({email: 'administrator', role: roles.ADMIN}));
    yield Info.create(InfoStub.getSingle());
    yield Question.insertMany(QuestionStub.getMany(4));
    yield AgeRange.insertMany(AgeRangeStub.getAgeRanges());
    yield Gender.insertMany(GenderStub.getGenders());
    yield Position.insertMany(PositionStub.getPositions());
    logger.info('Seed DB development completed');
  });
}

function seedDBProduction() {
  return co(function* () {
    yield clearDB();
    yield User.create(UserStub.getSingle({email: 'administrator', role: roles.ADMIN}));
    yield Info.create(InfoStub.getSingle());
    yield Question.insertMany(QuestionStub.getMany(4));
    yield AgeRange.insertMany(AgeRangeStub.getAgeRanges());
    yield Gender.insertMany(GenderStub.getGenders());
    yield Position.insertMany(PositionStub.getPositions());
    logger.info('Seed DB production completed');
  });
}

function clearDB() {
  return Promise.all([
    AgeRange.destroyAll(),
    Gender.destroyAll(),
    File.destroyAll(),
    Info.destroyAll(),
    Question.destroyAll(),
    User.destroyAll(),
    Position.destroyAll(),
  ]);
}
