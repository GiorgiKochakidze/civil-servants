'use strict';

const ejs = require('ejs');
const compression = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');


module.exports = (app) => {
  app.disable('x-powered-by');
  app.set('view engine', 'ejs');
  app.engine('.html', ejs.renderFile);
  app.use(compression());
  app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
  app.use(bodyParser.json({limit: '50mb'}));
  app.use(methodOverride());
  app.use(cookieParser());
};
