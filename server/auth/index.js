'use strict';

const co = require('co');
const jwt = require('jsonwebtoken');
const config = require('../config/environment');
const User = require('../api/users/user.dao');
const {roles} = require('../constants/user');
const {UnauthorizedError, ResourceNotFoundError} = require('../errors');


module.exports = {
  setUser,
  isSigned,
  isAdmin,
  signToken
};


function setUser(req, res, next) {
  co(function* () {
    const {token} = req.cookies;
    const {_id} = jwt.verify(token, config.jwt.secret);
    req.user = yield User.getById(_id);
    next();
  })
  .catch((e) => {
    if (e.name === 'TokenExpiredError' || e.name === 'JsonWebTokenError' || e instanceof ResourceNotFoundError) {
      req.user = {role: roles.GUEST};
      next();
    } else {
      next(e);
    }
  });
}

function isSigned(req, res, next) {
  const {_id, role} = req.user;
  if (role !== roles.GUEST) {
    next();
  } else {
    next(new UnauthorizedError(`User (id "${_id}") is not signed in`));
  }
}

function isAdmin(req, res, next) {
  const {_id, role} = req.user;
  if (role === roles.ADMIN) {
    next();
  } else {
    next(new UnauthorizedError(`User (id "${_id}") is not admin`));
  }
}

function signToken({_id}) {
  return jwt.sign({_id}, config.jwt.secret, {
    expiresIn: config.jwt.expiresIn
  });
}
