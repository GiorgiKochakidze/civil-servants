'use strict';

const _ = require('lodash');
const helpers = require('../helpers/stubHelpers');

const AgeRangeStub = {
  title: 'title'
};


module.exports = {
  getSingle,
  getMany,
  getAgeRanges,
};


function getSingle(fields) {
  return Object.assign(helpers.cloneStub(AgeRangeStub), fields);
}

function getMany(count, fields) {
  return _.range(count).map(i => Object.assign(
    getSingle(), {
      title: `title_${i}`
    }, fields)
  );
}

function getAgeRanges() {
  return [{
    title: {en: 'upward 55', ge: '55 წელს ზემოთ'},
    from: 55,
    to: 100
  }, {
    title: {en: 'between 35 - 55', ge: '35 - 55 წელს შორის'},
    from: 35,
    to: 55
  }, {
    title: {en: 'between 25 - 35', ge: '25 - 35 წელს შორის'},
    from: 25,
    to: 35
  }, {
    title: {en: 'between 18 - 25', ge: '18 - 25 წელს შორის'},
    from: 18,
    to: 25
  }, {
    title: {en: 'under 18', ge: '18 წელს ქვემოთ'},
    from: 0,
    to: 18
  }];
}