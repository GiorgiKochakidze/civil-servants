'use strict';

const _ = require('lodash');
const helpers = require('../helpers/stubHelpers');

const QuestionStub = {
  name: {ge: 'კითხვა', en: 'Question'},
  answers: []
};

module.exports = {
  getSingle,
  getMany
};


function getSingle(fields) {
  return Object.assign(helpers.cloneStub(QuestionStub), fields);
}

function getMany(count) {
  return _.range(count).map((i) => {
    const single = getSingle();
    single.name.ge += `-${i}`;
    single.name.en += `-${i}`;
    single.active = i === 0;
    single.answers = _.range(37).map((i) => {
      return {
        name: {ge: 'პასუხი' + i, en: 'Answer' + i},
        // respondents: _.range(_.floor(_.random(10, 100))).map(() => {
        //   return {firstName: 'Giorgi4', lastName: 'Kochakidze4', gender: 'male', age: 23};
        // })
        respondents: [],
      };
    });
    return single;
  });
}
