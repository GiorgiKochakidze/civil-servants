'use strict';

const _ = require('lodash');
const helpers = require('../helpers/stubHelpers');
const roles = require('../constants/user').roles;

const userStub = {
  facebookId: 'facebookId',
  firstName: 'Giorgi',
  lastName: 'Kochakdize',
  gender: 'male',
  age: 23,

  email: 'me@gmail.com',
  fullName: 'John Doe',
  passwordHash: '$2a$10$CWAGAtwAv2xdjnR.pzicqOipZwshQOtplPDRnsJ4gezFJBNTa/tcm',
  role: 'user',
  isEmailConfirmed: false,
  emailConfirmationToken: 'myEmailConfirmationToken',
  joinedAt: '2015-12-07T21:43:59.241Z'
};


module.exports = {
  getSingle,
  getMany
};


function getSingle(fields) {
  return Object.assign(helpers.cloneStub(userStub, ['email']), fields);
}

function getMany(count, fields) {
  return _.range(count).map(i =>
    Object.assign(getSingle(), {email: `user_${i}@gmail.com`}, fields));
}
