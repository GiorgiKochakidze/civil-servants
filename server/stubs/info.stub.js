'use strict';

var InfoStub = {
  about: {
    projectName: {
      ge: 'პროექტის სახელი',
      en: 'Project Name'
    },
    projectDescription: {
      ge: `მომზადებულია გაეროს განვითარების პროგრამის მიერ, გაერთიანებული სამეფოს მთავრობის მხარდაჭერით. გამოთქმული მოსაზრებები შეიძლება არ ასახავდეს გაეროს განვითარების პროგრამისა (UNDP) და გაერთიანებული სამეფოს მთავრობის თვალსაზრისს.`,
      en: `Developed by the United Nations Development Programme (UNDP) in collaboration with the Government of United Kingdom. Expressed views might not reflect those of UNDP and UK Government.`
    },
    statsDescription: {
      ge: `ეს გამოკითხვა წინ უძღვის დიდ კვლევას, რომელსაც ჩვენ ვამზადებთ საჯარო მმართველობის რეფორმის ფარგლებში.`,
      en: `This survey is an initial phase of big study which is prepared in scope of public management reform.`
    },
    footerDescription: {
      ge: `მომზადებულია გაეროს განვითარების პროგრამის მიერ, გაერთიანებული სამეფოს მთავრობის კარგი მმართველობის ფონდის მხარდაჭერით. გამოთქმული მოსაზრებები შეიძლება არ ასახავდეს გაეროს განვითარების პროგრამისა (UNDP) და გაერთიანებული სამეფოს მთავრობის თვალსაზრისს.`,
      en: `Developed by the United Nations Development Programme (UNDP) in collaboration with the UK’s Good Governance Fund”. Expressed views might not reflect those of UNDP and UK’s Good Governance Fund.`
    }
  }
};

module.exports = {
  getSingle
};

function getSingle() {
  return InfoStub;
}
