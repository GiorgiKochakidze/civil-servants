'use strict';

const _ = require('lodash');
const helpers = require('../helpers/stubHelpers');

const GenderStub = {
  name: 'name'
};


module.exports = {
  getSingle,
  getMany,
  getGenders,
};


function getSingle(fields) {
  return Object.assign(helpers.cloneStub(GenderStub), fields);
}

function getMany(count, fields) {
  return _.range(count).map(i => Object.assign(
    getSingle(), {
      name: `name_${i}`
    }, fields)
  );
}

function getGenders() {
  return [
    { name: { en: 'Female', ge: 'ქალი' } }, 
    { name: { en: 'Male', ge: 'კაცი' } }, 
  ];
}