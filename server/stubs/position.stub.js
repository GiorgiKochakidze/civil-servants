'use strict';

const _ = require('lodash');
const helpers = require('../helpers/stubHelpers');

const PositionStub = {
  name: 'name',
};


module.exports = {
  getSingle,
  getMany,
  getPositions,
};


function getSingle(fields) {
  return Object.assign(helpers.cloneStub(PositionStub), fields);
}

function getMany(count, fields) {
  return _.range(count).map(i => Object.assign(
    getSingle(), {
      name: `name_${i}`,
    }, fields)
  );
}

function getPositions() {
  return [{
    name: {en: 'position-en-1', ge: 'position-ge-1'},
    name: {en: 'position-en-2', ge: 'position-ge-2'},
    name: {en: 'position-en-3', ge: 'position-ge-3'}
  }];
}