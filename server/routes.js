'use strict';

const express = require('express');
const co = require('co');
const cheerio = require('cheerio');
const path = require('path');
const config = require('./config/environment');
const auth = require('./auth');
const logger = require('./helpers/logger');
const errors = require('./errors');
const Info = require('./api/info/info.dao');

module.exports = (app) => {
  app.get('/', renderIndexHtml);

  if (config.env === 'production') {
    app.use(express.static(path.join(config.root, 'dist')));
    app.use(express.static(path.join(config.paths.uploads)));
    app.set('views', 'dist');
  }

  if (config.env === 'development' || config.env === 'test') {
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(path.join(config.root, 'client')));
    app.use(express.static(path.join(config.root, './')));
    app.use(express.static(path.join(config.paths.uploads)));
    app.set('views', 'client');
  }

  app.use(auth.setUser);

  //inject:routes
  app.use('/api/ageRanges', require('./api/ageRanges'));
  app.use('/api/files', require('./api/files'));
  app.use('/api/genders', require('./api/genders'));
  app.use('/api/info', require('./api/info'));
  app.use('/api/positions', require('./api/positions'));
  app.use('/api/questions', require('./api/questions'));
  app.use('/api/users', require('./api/users'));
  //endinject

  app.post('/', renderIndexHtml);
  app.get('/*', renderIndexHtml);


  app.use(handleError);
};

function renderIndexHtml(req, res) {
  co(function* () {
    const lang = req.originalUrl.slice(1);

    if (lang === 'en' || lang === 'ge') {
      const {about} = yield Info.getOne();

      const ogTitle = about.projectName[lang];
      const ogDescription = cheerio.load(about.projectDescription[lang]).text();
      const ogUrl = `http://wordcloud.ge/${lang}`;

      res.render('index.html', {
        ogTitle,
        ogDescription,
        ogUrl
      });
    } else {
      res.render('index.html', {
        ogTitle: 'title',
        ogDescription: 'description',
        ogUrl: 'http://wordcloud.ge/ge'
      });
    }
  });
}

function handleError(err, req, res, next) {
  const status = Object(err).httpStatusCode || 500;
  res.sendStatus(status);

  if (err instanceof errors.AppError) {
    logger.error(err.message);
  } else {
    logger.error(err);
  }
}
