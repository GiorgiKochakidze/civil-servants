'use strict';

module.exports = (config) => {
  config.set({
    basePath: '',

    frameworks: ['jspm', 'jasmine'],

    files: [],

    jspm: {
      config: 'jspm.conf.js',
      loadFiles: ['.tmp/app/test/unit/**/*.js'],
      serveFiles: [
        '.tmp/app/**/*'
      ]
    },

    proxies: {
      '/.tmp/': '/base/.tmp/'
    },

    reporters: ['progress'],

    port: 3333,

    colors: true,

    logLevel: config.LOG_INFO,

    autoWatch: false,

    browsers: [
      'PhantomJS',
      //'Chrome',
      //'Firefox'
    ],

    singleRun: true
  });
};
