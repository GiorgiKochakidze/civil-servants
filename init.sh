
if [ $# -ne 1 ]
then
echo "Error! git repository is not provided"
exit 128
fi

echo $1
rm -rf .git gulp
git init
git submodule add git@github.com:vmaskhulia/gulp.git
git remote add origin $1
git add .
git commit -m "chore(init)"
git push origin master

