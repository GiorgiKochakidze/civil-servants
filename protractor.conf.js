'use strict';

exports.config = {
  specs: [
    'client/test/e2e/**/*.spec.js'
  ],

  seleniumServerJar: './node_modules/protractor/selenium/selenium-server-standalone-2.52.0.jar',

  framework: 'jasmine2',

  capabilities: {
    // browserName: 'chrome',
    browserName: 'phantomjs',

    'phantomjs.binary.path': require('phantomjs-prebuilt').path
  },

  onPrepare: () => {
    var disableNgAnimate = function () {
      angular.module('disableNgAnimate', []).run(function ($animate) {
        $animate.enabled(false);
      });
    };
    browser.addMockModule('disableNgAnimate', disableNgAnimate);
  },

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
