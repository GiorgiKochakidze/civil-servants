'use strict';

import 'font-awesome/css/font-awesome.min.css!';
import 'bootstrap/css/bootstrap.min.css!';
import 'angular-material/angular-material.css!';
import 'jqcloud2/dist/jqcloud.css!';

import 'spin.js';
import 'lodash';
import 'jquery';
import 'jqcloud';

import 'angular';
import 'angular-animate';
import 'angular-cookies/angular-cookies.min.js';
import 'angular-material';
import 'angular-messages/angular-messages.min.js';
import 'angular-sanitize/angular-sanitize.min.js';
import 'angular-translate/angular-translate.min.js';
import 'angular-ui-router/angular-ui-router.min.js';
import 'angular-bootstrap/ui-bootstrap-tpls.min.js';
import 'angular-spinner';
import 'ng-file-upload/dist/ng-file-upload.min.js';
import 'restangular/restangular.min.js';
import 'textAngular/dist/textAngular-sanitize';
import 'textAngular/dist/textAngularSetup';
import 'textAngular/dist/textAngular';

export default angular.module('dependencies', [
  'ngAnimate',
  'ngCookies',
  'ngMaterial',
  'ngMessages',
  'ngSanitize',
  'pascalprecht.translate',
  'ui.router',
  'ui.bootstrap',
  'angularSpinner',
  'ngFileUpload',
  'restangular',
  'textAngular'
]);
