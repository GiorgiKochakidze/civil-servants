'use strict';

import template from './login.html!text';
import controller from './login.controller.js';
import './login.css!';

export default angular.module('login', [])
  .config($stateProvider => {
    $stateProvider
      .state('login', {
        url: '/login',
        template,
        controller,
        controllerAs: 'vm'
      });
  });
