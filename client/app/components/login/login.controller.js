'use strict';

export default class {
  constructor($state, Auth, User) {
    'ngInject';

    this.$state = $state;
    this.Auth = Auth;
    this.User = User;
    this.user = {};
  }

  submit(form) {
    if (!form.$valid) {
      return;
    }
    this.submitted = true;
    this.User.signInAdmin(this.user)
      .then(() => {
        this.$state.go('admin.questions');
      })
      .catch(() => {
        this.errorVisible = true;
        this.submitted = false;
      });
  }
}
