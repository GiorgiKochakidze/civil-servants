'use strict';

import Login from './login/login';

import Main from './main/main';
//inject:import.main
import MainHome from './main/home/home.js';
import MainStats from './main/stats/stats.js';
import MainSurvey from './main/survey/survey.js';
//endinject

import Admin from './admin/admin';
//inject:import.admin
import AdminAgeRanges from './admin/ageRanges/ageRanges.js';
import AdminGenders from './admin/genders/genders.js';
import AdminInfo from './admin/info/info.js';
import AdminPositions from './admin/positions/positions.js';
import AdminQuestions from './admin/questions/questions.js';
import AdminSettings from './admin/settings/settings.js';
//endinject

export default angular.module('app.components', [
  Login.name,

  Main.name,
  //inject:ngmodule.main
  MainHome.name,
  MainStats.name,
  MainSurvey.name,
  //endinject

  Admin.name,
  //inject:ngmodule.admin
  AdminAgeRanges.name,
  AdminGenders.name,
  AdminInfo.name,
  AdminPositions.name,
  AdminQuestions.name,
  AdminSettings.name,
  //endinject
]);
