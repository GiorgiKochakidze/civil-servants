'use strict';

import * as _ from 'lodash';

export default class {
  constructor($state, $timeout, Question, Facebook, question, genders, ageRanges, positions) {
    'ngInject';

    this.$state = $state;
    this.$timeout = $timeout;
    this.Question = Question;
    this.Facebook = Facebook;

    this.question = question;
    this.genders = genders;
    this.ageRanges = ageRanges;
    this.positions = positions;
    this.answers = _.range(question.answers.length).map(() => false);
    this.user = {};
    this.showQuestion = true;
    this.showInfo = false;
  }

  submitAnswers() {
    if (this.anyAnswerChecked() && this.sufficientAnswersChecked()) {
      this.showQuestion = false;
      this.showInfo = true;
    } else {
      this.formSubmitted = false;
      this.$timeout(() => this.formSubmitted = true);
    }
  }

  submitInfo(valid) {
    if (!valid || this.requestInProgress) return;
    this.requestInProgress = true;
    let data = Object.assign(this.user, {answers: this.answers});
    this.Question.answer(this.question._id, data)
      .then(() => this.$state.go('stats'))
      .catch(() => this.requestInProgress = false)
  }

  sufficientAnswersChecked() {
    let checkedCount = 0;
    this.answers.forEach((checked) => {
      if (checked) {
        checkedCount++;
      }
    });
    return checkedCount < 4;
  }


  anyAnswerChecked() {
    return this.answers.some(
      answer => answer === true
    );
  }
}
