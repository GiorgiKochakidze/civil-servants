'use strict';

import template from './survey.html!text';
import controller from './survey.controller';
import './survey.css!';

export default angular.module('main.survey', [])
  .config($stateProvider => {
    $stateProvider
      .state('survey', {
        parent: 'main',
        url: 'survey',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {}
      });
  });
