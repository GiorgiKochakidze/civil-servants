'use strict';

import template from './home.html!text';
import controller from './home.controller';
import './home.css!';

export default angular.module('main.home', [])
  .config($stateProvider => {
    $stateProvider
      .state('home', {
        parent: 'main',
        url: '',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
        }
      });
  });
