'use strict';

import template from './main.html!text';
import controller from './main.controller';
import './main.css!';

export default angular.module('main', [])
  .config($stateProvider => {
    $stateProvider
      .state('main', {
        url: '/',
        abstract: true,
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          lang: (Lang) => Lang.getCurrent(),
          info: (Info) => Info.getOne(),
          question: (Question) => Question.getActiveQuestion(),
          genders: (Gender) => Gender.getByQuery({all: true}).then(({items}) => items),
          ageRanges: (AgeRange) => AgeRange.getByQuery({all: true}).then(({items}) => items),
          positions: (Position) => Position.getByQuery({all: true}).then(({items}) => items),
        }
      });
  });
