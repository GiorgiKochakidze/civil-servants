'use strict';

export default class {
  constructor($rootScope, $timeout, Lang, stats, info) {
    'ngInject';

    this.$timeout = $timeout;

    this.Lang = Lang;
    this.stats = stats;
    this.about = info.about;

    $timeout(() => {
      FB.XFBML.parse();
      this.initWordCloud();
    });

    $rootScope.$on('lang-change', () => {
      this.updateWordCloud();
    });
  }

  initWordCloud() {
    $('.word-cloud-w').jQCloud(
      this.cloudWords(),
      this.cloudColors()
    );
  }

  updateWordCloud() {
    $('.word-cloud-w').jQCloud(
      'update',
      this.cloudWords()
    );
  }

  cloudWords() {
    return this.stats.map((stat) => {
      return {
        text: stat.name[this.Lang.getCurrent()],
        weight: stat.respondents.length + stat.adminVotes
      };
    });
  }

  cloudColors() {
    return {
      colors: ['#ee1f40', '#119897', '#1C5193', '#5E84B3', '#f3637a', '#58B7B6'],
      autoResize: true,
      fontSize: {
        from: 0.06,
        to: 0.02
      }
    };
  }
}
