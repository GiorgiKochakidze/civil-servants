'use strict';

import template from './stats.html!text';
import controller from './stats.controller';
import './stats.css!';

export default angular.module('main.stats', [])
  .config($stateProvider => {
    $stateProvider
      .state('stats', {
        parent: 'main',
        url: 'stats',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          stats: (Question) => {
            return Question.getActiveQuestion()
              .then(question => question.answers);
          }
        }
      });
  });
