'use strict';

import AdminBaseController from '../AdminBaseController';

class gendersController extends AdminBaseController {
  constructor($state, Request, Gender, genderModal, confirmDestroyModal, query, gendersData) {
    'ngInject';
    super();

    this.$state = $state;
    this.Request = Request;
    this.Model = Gender;
    this.modal = genderModal;
    this.confirmDestroyModal = confirmDestroyModal;

    this.query = query;
    this.gendersData = gendersData;
  }

}

export default gendersController;
