'use strict';

import template from './genders.html!text';
import controller from './genders.controller';
import './genders.css!';

export default angular.module('admin.genders', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin.genders', {
        url: 'genders?searchText&page&limit&all',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          query: ($stateParams, QueryParser) =>
            QueryParser.parse($stateParams),

          gendersData: (Gender, query) =>
            Gender.getByQuery(query)
        }
      });
  });
