'use strict';

import template from './questions.html!text';
import controller from './questions.controller';
import './questions.css!';

export default angular.module('admin.questions', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin.questions', {
        url: 'questions?searchText&page&limit&all',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          query: ($stateParams, QueryParser) =>
            QueryParser.parse($stateParams),

          questionsData: (Question, query) =>
            Question.getByQuery(query)
        }
      });
  });
