'use strict';

import AdminBaseController from '../AdminBaseController';

class questionsController extends AdminBaseController {
  constructor($state, Request, Question, questionModal, confirmDestroyModal, query, questionsData) {
    'ngInject';
    super();

    this.$state = $state;
    this.Request = Request;
    this.Model = Question;
    this.modal = questionModal;
    this.confirmDestroyModal = confirmDestroyModal;

    this.query = query;
    this.questionsData = questionsData;
  }

  toggleStatus(model) {
    this.Request.send(
      this.Model.toggleStatus(model._id),
      () => this.reloadPage()
    );
  }

  exportToExcel(model) {
    this.Request.send(
      this.Model.exportToExcel(model._id),
      (file) => window.location.href = file.url
    );
  }

}

export default questionsController;
