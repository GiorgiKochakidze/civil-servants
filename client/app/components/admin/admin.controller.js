'use strict';

export default class {
  constructor($state, Auth, Request, Spinner) {
    'ngInject';

    this.$state = $state;
    this.Auth = Auth;
    this.Request = Request;

    this.spinnerKey = Spinner.getDefaultKey();
    this.navOpened = true;

    this.initAdminRoutes();
  }

  goToHome() {
    this.$state.go('home');
  }

  signOut() {
    this.Auth.signOut();
    this.$state.go('home');
  }

  toggleNavbar() {
    this.navOpened = !this.navOpened;
  }

  initAdminRoutes() {
    this.routes = [{
      sref: 'admin.questions',
      label: 'Survey Questions'
    }, {
      sref: 'admin.info',
      label: 'WebPage Content'
    }, {
      sref: 'admin.ageRanges',
      label: 'Age Ranges'
    }, {
      sref: 'admin.genders',
      label: 'Genders'
    }, {
      sref: 'admin.positions',
      label: 'Positions'
    }, {
      sref: 'admin.settings',
      label: 'Admin Parameters'
    }];
  }
}
