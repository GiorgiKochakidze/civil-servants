'use strict';

import _ from 'lodash';

export default class {
  create(event) {
    this.modal.open(event, {})
      .then((model) => {
        this.Request.send(
          this.Model.create(model)
        );
      });
  }

  update(event, model) {
    this.modal.open(event, model)
      .then((model) => {
        this.Request.send(
          this.Model.update(model)
        );
      });
  }

  destroy(event, model) {
    this.confirmDestroyModal.open(event)
      .then(() => {
        this.Request.send(
          this.Model.destroy(model._id)
        );
      });
  }

  searchByText() {
    this.query = _.pick(this.query, ['sort', 'searchText']);
    this.reloadPage();
  }

  toggleShowAllPages() {
    this.query = _.pick(this.query, ['all']);
    this.reloadPage();
  }

  reloadPage(page) {
    this.query.page = page || this.query.page;
    this.$state.go(this.$state.current, this.query, {inherit: false, reload: true});
  }
}
