'use strict';

import template from './settings.html!text';
import controller from './settings.controller';
import './settings.css!';

export default angular.module('admin.settings', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin.settings', {
        url: 'settings',
        template,
        controller,
        controllerAs: 'vm'
      });
  });
