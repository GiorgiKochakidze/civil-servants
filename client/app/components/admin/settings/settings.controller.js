'use strict';

class settingsController {
  constructor(Request, User, currentUser) {
    'ngInject';

    this.Request = Request;
    this.User = User;

    this.updatePasswordSchema = {
      current: '',
      new: ''
    };
  }

  submit(form) {
    if (form.$valid) {
      this.Request.send(
        this.User.updatePassword(this.updatePasswordSchema)
      );
    }
  }
}

export default settingsController;
