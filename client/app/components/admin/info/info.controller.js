'use strict';

const page = {
  selectedTab: 0
};

export default class {
  constructor(Request, Info, info) {
    'ngInject';

    this.Request = Request;
    this.Info = Info;

    this.info = info;
    this.page = page;
  }

  updateInfo(data) {
    this.Request.send(
      this.Info.update(data)
    );
  }
}
