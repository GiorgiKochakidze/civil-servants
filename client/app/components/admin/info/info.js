'use strict';

import template from './info.html!text';
import controller from './info.controller';
import './info.css!';

import AboutModule from './lcAbout/lcAbout';


export default angular.module('admin.info', [AboutModule.name])
  .config($stateProvider => {
    $stateProvider
      .state('admin.info', {
        url: 'info',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          info: (Info) => Info.getOne()
        }
      });
  });
