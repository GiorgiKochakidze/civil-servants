'use strict';

import template from './lcAbout.html!text';
import controller from './lcAbout.controller';
import './lcAbout.css!';

export default angular.module('lcAbout', [])
  .component('lcAbout', {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
      about: '=',
      onSubmit: '&'
    }
  });
