'use strict';

import _ from 'lodash';
import langs from '../../../../common/constants/lang';

export default class {
  constructor() {
    'ngInject';

    this.langs = _.cloneDeep(langs);
  }

  submit() {
    this.onSubmit({data: {about: this.about}});
  }
}
