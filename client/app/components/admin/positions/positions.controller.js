'use strict';

import AdminBaseController from '../AdminBaseController';

class positionsController extends AdminBaseController {
  constructor($state, Request, Position, positionModal, confirmDestroyModal, query, data) {
    'ngInject';
    super();

    this.$state = $state;
    this.Request = Request;
    this.Model = Position;
    this.modal = positionModal;
    this.confirmDestroyModal = confirmDestroyModal;

    this.query = query;
    this.data = data;
  }

}

export default positionsController;

