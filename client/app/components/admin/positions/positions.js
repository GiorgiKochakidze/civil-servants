'use strict';

import template from './positions.html!text';
import controller from './positions.controller';
import './positions.css!';

export default angular.module('admin.positions', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin.positions', {
        url: 'positions?searchText&page&limit&all',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          query: ($stateParams, QueryParser) => QueryParser.parse($stateParams),

          data: (Position, query) => Position.getByQuery(query)
        }
      });
  });
