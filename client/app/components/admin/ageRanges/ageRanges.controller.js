'use strict';

import AdminBaseController from '../AdminBaseController';

class ageRangesController extends AdminBaseController {
  constructor($state, Request, AgeRange, ageRangeModal, confirmDestroyModal, query, ageRangesData) {
    'ngInject';
    super();

    this.$state = $state;
    this.Request = Request;
    this.Model = AgeRange;
    this.modal = ageRangeModal;
    this.confirmDestroyModal = confirmDestroyModal;

    this.query = query;
    this.ageRangesData = ageRangesData;
  }

}

export default ageRangesController;
