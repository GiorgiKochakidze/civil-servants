'use strict';

import template from './ageRanges.html!text';
import controller from './ageRanges.controller';
import './ageRanges.css!';

export default angular.module('admin.ageRanges', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin.ageRanges', {
        url: 'ageRanges?searchText&page&limit&all',
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          query: ($stateParams, QueryParser) =>
            QueryParser.parse($stateParams),

          ageRangesData: (AgeRange, query) =>
            AgeRange.getByQuery(query)
        }
      });
  });
