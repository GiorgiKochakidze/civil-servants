'use strict';

import template from './admin.html!text';
import controller from './admin.controller';
import './admin.css!';

export default angular.module('admin', [])
  .config($stateProvider => {
    $stateProvider
      .state('admin', {
        url: '/admin/',
        abstract: true,
        template,
        controller,
        controllerAs: 'vm',
        resolve: {
          lang: (Lang) => Lang.getCurrent(),
          currentUser: ($q, User) => User.me()
            .then(user => user.isAdmin ? user : $q.reject('Unauthorized'))
        }
      });
  });
