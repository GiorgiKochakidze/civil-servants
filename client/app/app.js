'use strict';

import Dependencies from './app.dependencies.js';
import Common from './common/common.js';
import Components from './components/components.js';
import appComponent from './app.component.js';


const appModule = angular.module('app', [
  Dependencies.name,
  Common.name,
  Components.name
])
.component('app', appComponent);

angular.element(document).ready(() => {
  angular.bootstrap(document, [appModule.name]);
});

export default appModule;
