'use strict';

import {roles} from '../constants/user';

export default (Restangular) => {
  'ngInject';

  const rest = Restangular.withConfig((configurer) => {
    configurer.setBaseUrl('api/users');
  });

  return {
    me: () => rest.one('me').get().then(format),
    updatePassword: (data) => rest.one('password').post('update', data),
    signInAdmin: (data) => rest.one('sign-in').post('admin', data)
  };

  function format(item) {
    return Object.assign(item, {
      isGuest: item.role === roles.GUEST,
      isUser: item.role === roles.USER,
      isAdmin: item.role === roles.ADMIN
    });
  }
};
