'use strict';

export default (Restangular) => {
  'ngInject';

  const rest = Restangular.withConfig(configurer => {
    configurer.setBaseUrl('api/info');
  });

  return {
    getOne: () => {
      return rest.one('').get();
    },

    update: (data) => {
      return rest.one('update').post('', data);
    }
  };
};
