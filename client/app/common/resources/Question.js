'use strict';

export default (Restangular) => {
  'ngInject';

  const rest = Restangular.withConfig(configurer => {
    configurer.setBaseUrl('api/questions');
  });

  return {
    getByQuery: (query) => rest.one('').get(query)
      .then(({items, numTotal}) => ({
        items: items.map(format),
        numTotal
      })),

    getActiveQuestion: () => rest.one('active').get(),
    exportToExcel: (questionId) => rest.one(questionId, 'export').one('excel').get(),

    create: (data) => rest.one('').post('', data),
    update: (data) => rest.one('update').post('', data),
    toggleStatus: (questionId) => rest.one(questionId).post('toggleStatus'),
    answer: (questionId, data) => rest.one(questionId).post('answer', data),

    destroy: (id) => rest.one(id).remove()
  };

  function format(item) {
    return Object.assign(item, {});
  }
};
