'use strict';

export default (Upload) => {
  'ngInject';

  return {
    upload: (filesToAdd, filesToDestroy) =>
      Upload.upload({
        url: 'api/files',
        arrayKey: '',
        data: {
          filesToAdd,
          filesToDestroy
        }
      })
  };
};
