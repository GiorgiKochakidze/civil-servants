'use strict';

//inject:import
import AgeRange from './AgeRange.js';
import File from './File.js';
import Gender from './Gender.js';
import Info from './Info.js';
import Position from './Position.js';
import Question from './Question.js';
import User from './User.js';
//endinject

export default angular.module('resources', [])
  //inject:ngfactory
  .factory('AgeRange', AgeRange)
  .factory('File', File)
  .factory('Gender', Gender)
  .factory('Info', Info)
  .factory('Position', Position)
  .factory('Question', Question)
  .factory('User', User)
  //endinject
