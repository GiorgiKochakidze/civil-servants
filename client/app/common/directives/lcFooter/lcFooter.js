'use strict';

import template from './lcFooter.html!text';
import controller from './lcFooter.controller';
import './lcFooter.css!';

export default angular.module('lcFooter', [])
  .component('lcFooter', {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
      info: '='
    }
  });
