'use strict';

import template from './lcPagination.html!text';
import controller from './lcPagination.controller.js';
import './lcPagination.css!';

export default angular.module('lcPagination', [])
  .component('lcPagination', {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
      query: '=',
      numTotal: '=',
      reloadPage: '&'
    }
  });
