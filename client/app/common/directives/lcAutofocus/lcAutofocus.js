'use strict';

import lcAutofocus from './lcAutofocus.component';

export default angular.module('lcAutofocus', [])
  .directive('lcAutofocus', lcAutofocus);
