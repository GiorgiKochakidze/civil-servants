'use strict';

export default () => {
  'ngInject';

  return {
    link: ($scope, $element) => {
      setTimeout(() => {
        $element[0].focus();
      }, 100);
    }
  };
};
