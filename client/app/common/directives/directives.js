'use strict';

//inject:import
import lcAutofocus from './lcAutofocus/lcAutofocus.js';
import lcFooter from './lcFooter/lcFooter.js';
import lcHeader from './lcHeader/lcHeader.js';
import lcImageSelector from './lcImageSelector/lcImageSelector.js';
import lcPagination from './lcPagination/lcPagination.js';
//endinject

export default angular.module('directives', [
  //inject:ngmodule
  lcAutofocus.name,
  lcFooter.name,
  lcHeader.name,
  lcImageSelector.name,
  lcPagination.name,
  //endinject
]);
