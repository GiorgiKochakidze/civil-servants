'use strict';

import template from './lcHeader.html!text';
import controller from './lcHeader.controller';
import './lcHeader.css!';

export default angular.module('lcHeader', [])
  .component('lcHeader', {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {}
  });
