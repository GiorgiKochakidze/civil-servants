'use strict';

export default class {
  constructor(Upload) {
    'ngInject';

    this.Upload = Upload;

    window.URL = window.URL || window.webkitURL;
  }

  selectImages(files) {
    if (files && files.length) {
      files = files.map(f => this.Upload.rename(f, getFileName(f.name)));
      const urls = files.map(f => window.URL.createObjectURL(f));
      this.onSelect({files, urls});
    }
  }
}

function getFileName(oldName) {
  const ext = '.' + oldName.split('.').pop();
  return Math.random().toString(36).substring(7) + new Date().getTime() + ext;
}
