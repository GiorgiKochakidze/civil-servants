'use strict';

import template from './lcImageSelector.html!text';
import controller from './lcImageSelector.controller';
import './lcImageSelector.css!';

export default angular.module('lcImageSelector', [])
  .component('lcImageSelector', {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
      onSelect: '&'
    }
  });
