'use strict';

import config from './config/config.js';
import directives from './directives/directives.js';
import modals from './modals/modals.js';
import resources from './resources/resources.js';
import services from './services/services.js';

export default angular.module('app.common', [
  config.name,
  directives.name,
  modals.name,
  resources.name,
  services.name
]);
