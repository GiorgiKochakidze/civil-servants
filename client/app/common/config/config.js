'use strict';

import interceptorsConfig from './interceptors.config';
import routerConfig from './router.config';
import spinnerConfig from './spinner.config';
import stateConfig from './state.config';
import themeConfig from './theme.config';
import translateConfig from './translate.config';
import cookiesConfig from './cookies.config';

export default angular.module('config', [])
  .config(interceptorsConfig)
  .config(routerConfig)
  .config(spinnerConfig)
  .run(stateConfig)
  .config(themeConfig)
  .config(translateConfig)
  .config(cookiesConfig)
  .value('isDev', true)
  .value('host', 'http://wordcloud.ge/');
