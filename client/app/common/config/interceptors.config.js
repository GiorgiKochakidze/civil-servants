'use strict';

export default ($httpProvider) => {
  'ngInject';

  $httpProvider.interceptors.push(authInterceptor);
};

function authInterceptor($injector) {
  'ngInject';

  return {
    request: config => {
      const Auth = $injector.get('Auth');
      const token = Auth.getToken();
      config.headers = config.headers || {};
      if (token) {
        config.headers.Authorization = `Bearer ${token}`;
      }
      return config;
    }
  };
}
