'use strict';

export default ($rootScope, $state, $mdDialog) => {
  'ngInject';

  $rootScope.$on('$stateChangeError', (event, toState, toParams, fromState, fromParams, error) => {
    if (error.redirectTo) {
      return $state.go(error.redirectTo, {}, {reload: true, inherit: false});
    }
    console.error(error);
    switch (error) {
      case 'Unauthorized':
        $state.go('home', {}, {reload: true, inherit: false});
        break;
    }
  });

  $rootScope.$on('$stateChangeSuccess', () => {
    window.scrollTo(0, 0);
  });

  $rootScope.$on('$stateChangeStart', () => {
    $mdDialog.cancel();
  });
};
