'use strict';

export default ($translateProvider) => {
  'ngInject';

  $translateProvider
    .translations('ge', {
      ERROR_OCCURRED: 'მოხდა შეცდომა!',

      HOME: 'მთავარი',
      SURVEY: 'გამოკითხვა',
      STATS: 'სტატისტიკა',
      TAKE_SURVEY: 'გამოკითხვა',
      SUBMIT: 'დასრულება',
      WARNING: 'აირჩიეთ არა უმეტეს 3 პასუხისა',
      USER_INFO: 'შეავსეთ ინფორმაცია',
      GENDER: 'სქესი',
      AGE_RANGE: 'ასაკი',
      FIRST_NAME: 'სახელი',
      LAST_NAME: 'გვარი',
      POSITION: 'პოზიცია',
      
      T: '{{::ge}}'
    })
    .translations('en', {
      ERROR_OCCURRED: 'Error has occurred!',

      HOME: 'home',
      SURVEY: 'survey',
      STATS: 'stats',
      SUBMIT: 'Submit',
      TAKE_SURVEY: 'Take Survey',
      WARNING: 'Choose no more than 3 answers',
      USER_INFO: 'Fill Info',
      GENDER: 'Gender',
      AGE_RANGE: 'Age Range',
      FIRST_NAME: 'First Name',
      LAST_NAME: 'Last Name',
      POSITION: 'Position',

      T: '{{::en}}'
    })
    .useSanitizeValueStrategy(null)
    .preferredLanguage('ge');
};
