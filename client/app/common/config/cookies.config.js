'use strict';

export default ($cookiesProvider) => {
  'ngInject';

  const curDate = new Date();
  curDate.setFullYear(curDate.getFullYear() + 4);
  $cookiesProvider.defaults.expires = curDate;
};
