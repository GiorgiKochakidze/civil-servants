'use strict';

export default ($urlRouterProvider, $locationProvider) => {
  'ngInject';

  $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    })
    .hashPrefix('!');

  $urlRouterProvider.otherwise('/');
};
