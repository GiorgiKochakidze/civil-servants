'use strict';

export default (usSpinnerConfigProvider) => {
  'ngInject';

  const options = {
    lines: 13,
    length: 28,
    width: 14,
    radius: 42,
    scale: 1,
    corners: 1,
    color: '#009688',
    opacity: 0.25,
    rotate: 0,
    direction: 1,
    speed: 1,
    trail: 60,
    fps: 20
  };

  usSpinnerConfigProvider.setTheme('bigBlue', options);
};

