'use strict';


export const roles = {
  GUEST: 'guest',
  USER: 'user',
  ADMIN: 'admin'
};
