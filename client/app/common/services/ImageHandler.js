'use strict';

export default class {
  constructor(Upload) {
    'ngInject';

    this.Upload = Upload;

    window.URL = window.URL || window.webkitURL;
  }

  renameFile(file) {
    const newName = getFileName(file.name);
    return this.Upload.rename(file, newName);
  }

  createObjectURL(file) {
    return window.URL.createObjectURL(file);
  }
}

function getFileName(oldName) {
  const ext = '.' + oldName.split('.').pop();
  return Math.random().toString(36).substring(7) + new Date().getTime() + ext;
}
