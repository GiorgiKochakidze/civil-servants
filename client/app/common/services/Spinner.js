'use strict';

const DEFAULT_KEY = 'default-spinner';

export default class {
  constructor(usSpinnerService) {
    'ngInject';

    this.usSpinnerService = usSpinnerService;
  }

  getDefaultKey() {
    return DEFAULT_KEY;
  }

  spin() {
    this.usSpinnerService.spin(DEFAULT_KEY);
  }

  stop() {
    this.usSpinnerService.stop(DEFAULT_KEY);
  }
}
