'use strict';

import langs from '../constants/lang';
const LOCAL_LANG_KEY = 'local-lang';

export default class {
  constructor($translate, $cookies, $rootScope) {
    'ngInject';

    this.$translate = $translate;
    this.$cookies = $cookies;
    this.$rootScope = $rootScope;

    const lang = this.getCurrent() || langs.GE;

    this.use(lang);
  }

  getCurrent() {
    return this.$cookies.get(LOCAL_LANG_KEY);
  }

  use(lang) {
    this.$translate.use(lang);
    this.$cookies.put(LOCAL_LANG_KEY, lang);
    this.$rootScope.$emit('lang-change', lang);
  }
}
