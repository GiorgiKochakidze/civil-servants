'use strict';

export default class {
  constructor(isDev) {
    'ngInject';

    this.isDev = isDev;
  }

  getMultilingualSchema() {
    return {
      ge: '',
      en: '',
      ru: ''
    };
  }
}
