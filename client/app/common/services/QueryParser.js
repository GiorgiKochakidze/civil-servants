'use strict';

import {limit} from '../constants/query';

export default class {
  parse(params, customParser) {
    customParser = customParser || ((params, parsed) => parsed);

    const parsed = {
      sort: params.sort,
      searchText: params.searchText,
      page: Number(params.page) || 1,
      limit: Number(params.limit) || limit,
      all: params.all === 'true'
    };

    return customParser(params, parsed);
  }
}
