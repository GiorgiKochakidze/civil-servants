'use strict';

//inject:import
import Auth from './Auth.js';
import Facebook from './Facebook.js';
import ImageHandler from './ImageHandler.js';
import Lang from './Lang.js';
import QueryParser from './QueryParser.js';
import Request from './Request.js';
import Schemas from './Schemas.js';
import Spinner from './Spinner.js';
import Toast from './Toast.js';
//endinject

export default angular.module('services', [])
  //inject:ngservice
  .service('Auth', Auth)
  .service('Facebook', Facebook)
  .service('ImageHandler', ImageHandler)
  .service('Lang', Lang)
  .service('QueryParser', QueryParser)
  .service('Request', Request)
  .service('Schemas', Schemas)
  .service('Spinner', Spinner)
  .service('Toast', Toast)
  //endinject
