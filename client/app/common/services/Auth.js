'use strict';

const TOKEN_KEY = 'token';

export default class {
  constructor($cookies) {
    'ngInject';

    this.$cookies = $cookies;
  }

  getToken() {
    return this.$cookies.get(TOKEN_KEY);
  }

  signOut() {
    this.$cookies.remove(TOKEN_KEY);
  }
}
