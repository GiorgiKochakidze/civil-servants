'use strict';

export default class {
  constructor($q) {
    'ngInject';

    this.$q = $q;
  }

  getUser() {
    return this.$q((resolve) => {
      FB.getLoginStatus((response) => {
        if (response.status === 'connected') {
          resolve(response);
        } else {
          FB.login(resolve, {scope: 'user_birthday'});
        }
      });
    });
  }
}
