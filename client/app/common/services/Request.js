'use strict';

export default class {
  constructor($state, $translate, Spinner) {
    'ngInject';

    this.$state = $state;
    this.$translate = $translate;
    this.Spinner = Spinner;
  }

  send(request, success, error) {
    success = success || this.getDefaultSuccess(this.$state);
    error = error || this.getDefaultError(this.$translate.instant('ERROR_OCCURRED'));

    this.Spinner.spin();

    return request
      .finally(() => {
        this.Spinner.stop();
      })
      .then(success)
      .catch(error);
  }

  getDefaultSuccess($state) {
    return () => {
      $state.reload();
    };
  }

  getDefaultError(errorMsg) {
    return () => {
      window.alert(errorMsg);
    };
  }
}
