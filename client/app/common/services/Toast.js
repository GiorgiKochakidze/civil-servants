'use strict';

const HIDE_DELAY = 1500;

export default class {
  constructor($mdToast) {
    'ngInject';

    this.$mdToast = $mdToast;
  }

  show(content) {
    this.$mdToast.show(
      this.$mdToast.simple()
        .textContent(content)
        .position('top right')
        .hideDelay(HIDE_DELAY)
    );
  }
}
