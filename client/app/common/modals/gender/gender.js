'use strict';

import _ from 'lodash';
import template from './gender.html!text';
import './gender.css!';

import langs from '../../constants/lang';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent, gender) {
    return this.$mdDialog.show({
      controller($mdDialog) {
        'ngInject';

        this.gender = _.cloneDeep(gender);
        this.langs = langs;
        
        this.submit = (isValid) => {
          if (isValid) {
            $mdDialog.hide(this.gender);
          }
        };

        this.close = () => {
          $mdDialog.cancel();
        };
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
