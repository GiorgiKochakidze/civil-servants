'use strict';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(event) {
    const confirm = this.$mdDialog.confirm()
      .title('წაშლის დადასტურება')
      .content('წაშლის შემთხვევაში ინფორმაციის აღდგენა შეუძლებელი იქნება!')
      .ok('დადასტურება')
      .cancel('გაუქმება')
      .clickOutsideToClose(true)
      .targetEvent(event);

    return this.$mdDialog.show(confirm);
  }
}
