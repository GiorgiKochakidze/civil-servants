'use strict';

//inject:import
import ageRange from './ageRange/ageRange.js';
import confirmDestroy from './confirmDestroy/confirmDestroy.js';
import gallery from './gallery/gallery.js';
import gender from './gender/gender.js';
import position from './position/position.js';
import question from './question/question.js';
import recoverPassword from './recoverPassword/recoverPassword.js';
import signIn from './signIn/signIn.js';
import signUp from './signUp/signUp.js';
//endinject

export default angular.module('modals', [])
  //inject:ngservice
  .service('ageRangeModal', ageRange)
  .service('confirmDestroyModal', confirmDestroy)
  .service('galleryModal', gallery)
  .service('genderModal', gender)
  .service('positionModal', position)
  .service('questionModal', question)
  .service('recoverPasswordModal', recoverPassword)
  .service('signInModal', signIn)
  .service('signUpModal', signUp)
  //endinject
