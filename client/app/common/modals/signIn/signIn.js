'use strict';

import template from './signIn.html!text';
import './signIn.css!';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent) {
    return this.$mdDialog.show({
      controller($mdDialog, User, recoverPasswordModal) {
        'ngInject';

        this.user = {};

        this.submit = () => {
          User.signIn(this.user)
            .then(() => {
              $mdDialog.hide();
            })
            .catch(() => {
              this.errorVisible = true;
            });
        };

        this.openRecoverPasswordModal = (event) => {
          recoverPasswordModal.open(event);
        };

        this.close = () => {
          $mdDialog.cancel();
        };
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
