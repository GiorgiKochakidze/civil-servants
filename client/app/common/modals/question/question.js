'use strict';

import _ from 'lodash';
import template from './question.html!text';
import './question.css!';
import langs from '../../constants/lang';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent, question) {
    return this.$mdDialog.show({
      controller($mdDialog) {
        'ngInject';

        this.question = _.cloneDeep(question);
        this.question.answers = this.question.answers || [];
        this.langs = langs;
        this.newAnswer = {name: {}, voters: []};

        this.removeAnswer = (index) => {
          this.question.answers.splice(index, 1);
          this.newAnswer = {name: {}, voters: []};
        };

        this.addAnswer = () => {
          this.question.answers.unshift(this.newAnswer);
          this.newAnswer = {name: {}, voters: []};
        };

        this.submit = (isValid) => {
          if (isValid) {
            $mdDialog.hide(this.question);
          }
        };

        this.close = () => {
          $mdDialog.cancel();
        };
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
