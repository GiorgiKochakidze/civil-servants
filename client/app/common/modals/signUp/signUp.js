'use strict';

import template from './signUp.html!text';
import './signUp.css!';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent) {
    return this.$mdDialog.show({
      controller($mdDialog, Request, User) {
        'ngInject';

        this.user = {};

        this.submit = (form) => {
          if (form.$valid) {
            Request.send(
              User.create(this.user),
              () => {
                $mdDialog.hide();
              }
            );
          }
        };

        this.close = () => {
          $mdDialog.cancel();
        };
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
