'use strict';

import _ from 'lodash';
import template from './position.html!text';
import './position.css!';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent, position) {
    return this.$mdDialog.show({
      controller($mdDialog) {
        'ngInject';

        this.position = _.cloneDeep(position);

        this.submit = (isValid) => {
          if (isValid) {
            $mdDialog.hide(this.position);
          }
        };

        this.close = $mdDialog.cancel;
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
