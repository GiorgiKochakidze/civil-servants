'use strict';

import template from './recoverPassword.html!text';
import './recoverPassword.css!';

export default class {
  constructor($uibModal) {
    'ngInject';

    this.$uibModal = $uibModal;
  }

  open() {
    return this.$uibModal.open({
      controller($uibModalInstance, User) {
        'ngInject';

        this.user = {};

        this.submit = (form) => {
          form.email.$setValidity('exists', true);
          if (form.$valid) {
            User.recoverPassword(this.user.email)
              .then(() => {
                this.close();
              })
              .catch(() => {
                form.email.$setValidity('exists', false);
              });
          }
        };

        this.close = () => {
          $uibModalInstance.dismiss('cancel');
        };
      },
      controllerAs: 'vm',
      template,
      size: 'sm'
    }).result;
  }
}
