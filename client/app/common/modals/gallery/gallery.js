'use strict';

import template from './gallery.html!text';
import './gallery.css!';

export default class {
  constructor($uibModal) {
    'ngInject';

    this.$uibModal = $uibModal;
  }

  open() {
    return this.$uibModal.open({
      resolve: {
        allImages: (Image) => {
          'ngInject';
          return Image.getAll();
        }
      },
      controller($uibModalInstance, Request, Image, ImageUploader, allImages) {
        'ngInject';

        this.page = 1;
        this.limit = 3;
        this.images = allImages.slice(0, this.limit);
        this.allImages = allImages;

        this.uploadImage = (files) => {
          if (files && files.length) {
            Request.send(
              ImageUploader.upload(files),
              (images) => {
                allImages.push(...images);
                var page = Math.ceil(allImages.length / this.limit);
                this.setPage(page);
              }
            );
          }
        };

        this.destroyImage = (image) => {
          Request.send(
            Image.destroy(image._id),
            () => {
              allImages.splice(allImages.indexOf(image), 1);
              this.setPage(this.page);
            }
          );
        };

        this.chooseImage = (image) => {
          $uibModalInstance.close(image);
        };

        this.toggleShowAll = () => {
          if (this.showAll) {
            this.images = allImages;
          } else {
            this.setPage(1);
          }
        };

        this.setPage = (page) => {
          var from = (page - 1) * this.limit;
          var to = from + this.limit;
          this.images = allImages.slice(from, to);
          this.page = page;
        };

        this.close = () => {
          $uibModalInstance.dismiss('cancel');
        };
      },
      controllerAs: 'vm',
      template,
      size: 'lg'
    }).result;
  }
}
