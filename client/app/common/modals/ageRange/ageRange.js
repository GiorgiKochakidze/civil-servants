'use strict';

import _ from 'lodash';
import template from './ageRange.html!text';
import './ageRange.css!';

import langs from '../../constants/lang';

export default class {
  constructor($mdDialog) {
    'ngInject';

    this.$mdDialog = $mdDialog;
  }

  open(targetEvent, ageRange) {
    return this.$mdDialog.show({
      controller($mdDialog) {
        'ngInject';

        this.ageRange = _.cloneDeep(ageRange);
        this.langs = langs;

        this.submit = (isValid) => {
          if (isValid) {
            $mdDialog.hide(this.ageRange);
          }
        };

        this.close = () => {
          $mdDialog.cancel();
        };
      },
      controllerAs: 'vm',
      template,
      targetEvent,
      clickOutsideToClose: true
    });
  }
}
